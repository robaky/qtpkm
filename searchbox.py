#! /usr/bin/env python3

import sys

import numpy as np

from PyQt6 import *
from PyQt6.QtGui import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import QStringListModel


from pyqtgraph.Qt import QtCore
import pyqtgraph as pg

from messages import Select, Open, DataChanged



class SearchBox(QWidget):
    subscribe_to_msgs = [Select, DataChanged]

    def __init__(self, data_manager):
        super().__init__()
        self.data_manager = data_manager

        self.searchbar = QLineEdit()
        self.completer = QCompleter(self.data_manager.meta_data.index)
        self.searchbar.setCompleter(self.completer)
        input_validator = ItemIdValidator(data_manager)
        self.searchbar.setValidator(input_validator)

        self.citing = QListWidget()
        self.cited = QListWidget()

        self.searchbar.returnPressed.connect(self.send_select)

        self.cited.itemClicked.connect(self.clicked_item)
        self.citing.itemClicked.connect(self.clicked_item)

        layout = QVBoxLayout()
        layout.addWidget(self.searchbar)
        layout.addWidget(self.cited)
        layout.addWidget(self.citing)
        self.setLayout(layout)


    def __repr__(self):
        return 'search_box'

    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus
        self.comm_nexus.register(self, self.subscribe_to_msgs)

    def on_message(self, message):
        if type(message) is Select:
            self.populate_lists(message.item_id)
            self.searchbar.clear()

        if isinstance(message, DataChanged):
            self.update_completer()

    def update_completer(self):
        self.completer.setModel(
            QStringListModel(self.data_manager.meta_data.index))

    def send_select(self):
        self.comm_nexus.on_message(
            Select(
                self.searchbar.text(),
                str(self)
            ))

    def populate_lists(self, item_id):
        '''  '''
        self.citing.clear()
        self.cited.clear()

        self.citing.addItems(self.data_manager.get_citing(item_id))
        self.cited.addItems(self.data_manager.get_cited(item_id))

    def clicked_item(self, event):
        if self.data_manager.get_items_type(event.text()) == 'note':
            self.comm_nexus.on_message(
                Select(
                    event.text(),
                    str(self)
                )
            )


class ItemIdValidator(QValidator):
    def __init__(self, data_manager):
        super().__init__()
        self.data_manager = data_manager

    def validate(self, item, pos):
        if item in self.data_manager.meta_data.index:
            return (QValidator.State.Acceptable, item, pos)
        else:
            return (QValidator.State.Intermediate, item, pos)

