#! /usr/bin/env python3
import shutil
import os

os.remove('graph_layout.json')
os.remove('graph_layout.json.bckp')
os.remove('meta.pkl')
os.remove('meta.pkl.bckp')
shutil.rmtree('pkm_store/')
os.mkdir('pkm_store')
