#! /usr/bin/env python3

import os
from datetime import date
from collections import Counter
import shutil
import re
from copy import deepcopy
import pandas as pd
import numpy as np
import json
import networkx as nx
from bibtexparser.bparser import BibTexParser
from bibtexparser.bibdatabase import BibDatabase
from sklearn.cluster import AgglomerativeClustering

import io

#import pyarrow as pa
#import pyarrow.parquet as pq

from messages import *
from fields import *


def flatten_list(list_of_lists):
    return [
        item for sublist in list_of_lists
        for item in sublist
    ]


class DataManager:
    subscribe_to_msgs = [
        CreateItem,
        UpdateItemsMeta,
        UpdateNote,
        GenerateId,
        UpdatePositions,
        DeleteItems,
    ]

    meta_cols = {
        'item_id':str,
        'title':str,
        'creators':object, # list
        'tags':object, # set
        'type':str,
        'read_flag':bool,
        'to_read_flag':bool,
        'date_published':object,
        'date_added':object,
        'cited':object, # set
        'bibtex':object, # dict
        'file':str,
        'size':float,
        'url':str,
    }

    meta_cols_true_dtypes = {
        'item_id':str,
        'title':str,
        'creators':list,
        'tags':set,
        'type':str,
        'read_flag':bool,
        'to_read_flag':bool,
        'date_published':date,
        'date_added':date,
        'cited':set,
        'bibtex':BibDatabase,
        'file':str,
        'size':float,
        'url':str,
    }

    req_cols = ['item_id', 'title', 'type']

    item_types = ['note', 'web', 'text', 'audio', 'video', 'graphics']
    item_types_w_files = ['text', 'audio', 'video', 'graphics']

    citation_pattern = r'[\[[][^]]*?[\]]]'

    node_marker_map = {
        'note':'o',
        'web':'h',
        'text':'t1',
        'audio':'p',
        'video':'p',
        'graphics':'p',
    }

    @staticmethod
    def sanitize_id(_id):
        return re.sub(r"[^a-zA-Z0-9 ]+", '', _id).strip().replace(' ', '')


    def __init__(self, config):
        self.config = config

        try:
            self.load_meta_data(config)

        except FileNotFoundError:
            self.init_meta_data()

        try:
            with open(config.graph) as fg:
                self.graph = nx.node_link_graph(json.load(fg))
        except FileNotFoundError:
            self.init_graph()


    def __repr__(self):
        return 'DataManager'


    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus
        self.comm_nexus.register(self, self.subscribe_to_msgs)


    def on_message(self, message):
        if type(message) == CreateItem:
            item_id = self.create_item(message.meta)
            self.send_simple_message(ItemCreated, item_id)

        elif type(message) == UpdateItemsMeta:
            self.update_metadata(message.item_id, message.meta)
            self.send_simple_message(ItemsMetaUpdated, message.item_id)

        elif type(message) == UpdateNote:
            self.update_note(message.item_id, message.text)
            self.send_simple_message(NoteUpdated, message.item_id)

        elif type(message) == MergeItems:
            self.merge_items(message.item_ids)
            self.send_multi_message(ItemsMerged, message.item_ids)

        elif type(message) == DeleteItems:
            self.delete_items(message.item_ids)
            self.send_multi_message(ItemsDeleted, message.item_ids)

        elif type(message) == UpdatePositions:
            nx.set_node_attributes(self.graph, message.positions)

        elif type(message) == RenameTag:
            self.rename_tag(message.old_tag, message.new_tag)
            self.comm_nexus.on_message(
                TagRenamed(
                   old_tag = message.old_tag,
                   new_tag = message.new_tag,
                   sender = str(self)
               )
            )


        else:
            raise ValueError('message type not recognized: %s' % message)


    def send_simple_message(self, msg_type, item_id):
        self.comm_nexus.on_message(
            msg_type(
               item_id = item_id,
               sender = str(self)
           )
        )

    def send_multi_message(self, msg_type, item_ids):
        self.comm_nexus.on_message(
            msg_type(
               item_ids = item_ids,
               sender = str(self)
           )
        )


    def create_item(self, meta):
        '''  '''
        # generate unique id
        if not self.check_id_suffix_exists(meta['item_id']):
            id_sfx = self.generate_id_suffix(meta['item_id'])
            meta['item_id'] = meta['item_id'] + '#' + id_sfx

        # create item's folder
        item_path = self.generate_folder_path(meta['item_id'])

        if not os.path.exists(item_path):
            os.mkdir(item_path)

        # handle item's file(s)
        if 'note_text' in meta.keys():
            note_path = os.path.join(item_path, 'note')

            with open(note_path, 'w+') as f_out:
                f_out.writelines(meta['note_text'])

        elif 'path' in meta.keys():
            # universal for all files
            file_ext = meta['path'].split('.')[-1]

            file_path = os.path.join(item_path, 'file.%s' % file_ext)
            shutil.copy2(meta['path'], file_path)
            meta['file'] = 'file.%s' % file_ext

        elif 'url' in meta.keys():
            # store locally blog posts, etc.
            self.get_stuff_from_url(meta['url'], item_path)
            meta['file'] = 'index.html'
            meta['url'] = meta['url'] # saving for future ref

        else:
            pass

        # extract cited if adding note
        # provide filename for consistency
        if 'note_text' in meta.keys():
            cited = self.extract_citations(meta['note_text'])
            meta['cited'] = cited
            meta['file'] = 'note'

        # parse bibtex metadata if present
        if 'bibtex' in meta.keys():
            if type(meta['bibtex']) is str:
                meta['bibtex'] = BibtexField.transform(
                    meta['bibtex'],
                    'table',
                    'meta_data'
                )

            elif type(meta['bibtex']) is BibDatabase:
                pass

            else:
                raise TypeError(
                    'bibtex field has unrecognized type: %s' % type(
                        meta['bibtex']))

        # create empty cited to avoid problems
        if not 'cited' in meta.keys():
            meta['cited'] = set([])

        if not type(meta['cited']) is set:
            raise TypeError('cited field has unrecognized type: %s' % type(
                meta['cited']))

        # get the size if applicable
        if 'file' in meta.keys():
            new_size = self.calc_size(meta['item_id'], meta['file'])
            meta['size'] = new_size

        else:
            meta['size'] = np.nan

        # add 'added date' if missing:
        if not 'date_added' in meta.keys():
            meta['date_added'] = date.today()

        if not 'date_published' in meta.keys():
            meta['date_published'] = date.min

        # fill in empty fields, taking into account dtypes
        to_fill = set(self.meta_cols) - set(meta.keys())

        for c in to_fill:
            meta[c] = self.meta_cols_true_dtypes[c]()

        # save to meta_data
        cols_to_save = set(meta.keys()) & set(self.meta_cols)
        meta_red = {k: meta[k] for k in cols_to_save}
        meta_df = pd.DataFrame([meta_red]).set_index('item_id')

        self.meta_data = pd.concat([
            self.meta_data,
            meta_df,
        ])

        self.graph.add_node(meta['item_id'], title=meta['title'])

        for c in meta['cited']:
            self.graph.add_edge(meta['item_id'], c)

        pos = self.generate_random_positions([meta['item_id']])
        nx.set_node_attributes(self.graph, pos)

        # save to files
        self.save()

        return meta['item_id']


    def update_metadata(self, item_id, new_meta):
        flag_update_id = False
        old_meta = self.meta_data.loc[item_id].copy()

        if 'item_id' in new_meta.keys():
            if item_id != new_meta['item_id']:
                flag_update_id = True

        if flag_update_id:
            # ! only item_id is handled, rest is ignored
            # metadata must be updated separately

            # fix id, if lacks the suffix
            if not self.check_id_suffix_exists(new_meta['item_id']):
                new_meta['item_id'] += '#' + self.generate_id_suffix(
                    new_meta['item_id']
                )

            # remove item
            self.meta_data = self.meta_data.drop(item_id)
            self.graph.remove_node(item_id)

            # rename the folder
            shutil.move(
                self.generate_folder_path(item_id),
                self.generate_folder_path(new_meta['item_id']),
            )

            # create a new item
            # this fixes both table and graph
            self.create_item(new_meta)

        else:
            # cited, file and url need special treatment

            # updated graph conns if necessary
            if 'cited' in new_meta.keys():
                if new_meta['cited'] != old_meta['cited']:
                    # remove old
                    tmp_e = list(self.graph.edges(item_id))
                    self.graph.remove_edges_from(tmp_e)
                    # add new
                    new_e = [
                        (item_id, t) for t in new_meta['cited']]
                    self.graph.add_edges_from(new_e)

            # update the file if necessary:
            old_path = self.generate_item_file_path(item_id)

            if 'file' in new_meta.keys():
                # otherwise it's just an old filename
                if len(new_meta['file'].split('/')) > 1:
                    if old_path != new_meta['file']:
                        os.remove(self.generate_item_file_path(item_id))

                        folder_path = self.generate_folder_path(item_id)
                        shutil.copy2(new_meta['file'], old_path)

                        self.meta_data.loc[item_id, 'file'] = \
                            new_meta['file'].split('/')[-1]

            # update the url if necessary
            if 'url' in new_meta.keys():
                if new_meta['url'] != self.meta_data.loc[item_id, 'url']:
                    os.remove(self.generate_item_file_path(item_id))
                    item_path = self.generate_folder_path(new_meta['item_id'])
                    self.get_stuff_from_url(new_meta['url'], item_path)

                    self.meta_data.loc[item_id, 'file'] = 'index.html'
                    self.meta_data.loc[item_id, 'url'] = meta['url']

            # deal with the rest of fields
            tmp_cols = set(self.meta_cols.keys()) & set(new_meta.keys())
            # file was dealth with; this function does not handle notes txt
            # see update_note() for that
            tmp_cols -= {'file', 'url', 'note_text'}

            # update the rest
            for c in tmp_cols:
                self.meta_data.at[item_id, c] = new_meta[c]

            # update the size
            f_path = self.generate_folder_path(
                self.meta_data.loc[item_id, 'file']
            )

            new_size = self.calc_size(item_id)
            self.meta_data.loc[item_id, 'size'] = new_size


    def get_stuff_from_url(self, url, item_path):
        os.system('wget %s -O %s > /dev/null 2>&1' %
            (url, os.path.join(item_path, 'index.html'))
        )


    def update_note(self, item_id, text):
        cited = self.extract_citations(text)

        # update citations if necessary
        if cited != self.meta_data.loc[item_id, 'cited']:
            new_cits = cited - self.meta_data.loc[item_id, 'cited']
            rem_cits = self.meta_data.loc[item_id, 'cited'] - cited

            new_cits = [(item_id, c) for c in new_cits]
            rem_cits = [(item_id, c) for c in rem_cits]

            self.graph.remove_edges_from(rem_cits)
            self.graph.add_edges_from(new_cits)

            self.meta_data.at[item_id, 'cited'] = cited

        # update the note file
        note_path = os.path.join(
            self.generate_folder_path(item_id),
            self.meta_data.loc[item_id, 'file']
        )

        with open(note_path, 'w') as note_f:
            note_f.write(text)

        # update the size
        new_size = self.calc_size(item_id)
        self.meta_data.loc[item_id, 'size'] = new_size


    def calc_size(self, item_id, file=None):
        ''' based on the size of files in bytes '''
        file_path = self.generate_item_file_path(item_id, file=file)
        return os.path.getsize(file_path)


    def get_reference_sizes(self):
        ref_sizes = {}
        for type_ in self.meta_data['type'].unique():
            tmp_data = self.meta_data[self.meta_data['type'] == type_]
            ref_sizes[type_] = {
                'min':tmp_data['size'].min(),
                'max':tmp_data['size'].max(),
            }

        return ref_sizes


    # TODO these two functions not needed actually?
    def remove_citation(self, citing_id, cited_id):
        self.meta_data.loc[citing_id, 'cited'].remove(cited_id)
        self.graph.remove_edge(citing_id, cited_id)

    def add_citation(self, citing_id, cited_id):
        self.meta_data.loc[citing_id, 'cited'].add(cited_id)
        self.graph.add_edge(citing_id, cited_id)


    def merge_items(self, items_ids):
        new_item_id = sorted(items_ids)[0]
        new_meta = {'item_id':new_item_id}

        # keep the most popular non-empty meta
        for c in self.meta_cols:
            vals = [self.meta_data.loc[i, c] for i in items_ids]
            vals = [v for v in vals if not v in [(), {}, [], '']]
            if vals == []:
                continue
            else:
                new_meta[c] = self.merge_values(vals)

        # remove all merged items
        self.delete_items(items_ids)

        # create the replacement item
        self.create_item(new_meta)


    def check_mergeability(self, items_ids):
        pass


    def delete_items(self, items_ids):
        # could instead put items to 'bin' first
        # to avoid problems
        self.graph.remove_nodes_from(items_ids)
        for item_id in items_ids:
            self.meta_data.drop(item_id, inplace=True)
            shutil.rmtree(self.generate_folder_path(item_id))

    def merge_values(self, vals):
        count = Counter(vals)
        return count.most_common(1)[0][0]


    def rename_tag(self, old_tag, new_tag):
        s = self.meta_data['tags'].copy()
        
        def update(x):
            if old_tag in x:
                x.remove(old_tag)
                x.add(new_tag)
            else:
                pass

        s.map(lambda x: update(x))

        self.meta_data['tags'] = s


    def generate_folder_path(self, item_id):
        return os.path.join(self.config.pkm_store, item_id)

    def generate_note_path(self, item_id):
        path = self.generate_folder_path(item_id)
        return os.path.join(path, 'note')

    def generate_item_file_path(self, item_id, file=None):
        path = self.generate_folder_path(item_id)

        if file is None:
            return os.path.join(path, self.meta_data.loc[item_id, 'file'])
        else:
            return os.path.join(path, file)


    def extract_data_from_bibtex_str(self, bib):
        ''' accepts a raw bibtex string
            assumes only 1 entry in the string
        '''

        bp = BibTexParser(interpolate_strings=False)
        return bp.parse(bib).entries[0]


    def generate_id_from_bibtex(self, bib):
        ''' generate unique id from bibtex info

            bib [dict] : parsed bibtex entry

            returns [str]: an id in format:
            <author><date><title>

            ^ author and title are just first
            available words
        '''

        _id = ''.join([
            bib['author'].split(',')[0],
            bib['year'],
            bib['title'].split(' ')[0]
        ])

        _id = self.sanitize_id(_id)

        sfx = self.generate_id_suffix(_id)
        full_key = '#'.join([_id, sfx])

        return full_key


    def generate_id_suffix(self, id_pref):
        tmp = [i for i in self.meta_data.index if i.startswith(id_pref)]
        tmp = [int(i.split('#')[-1]) for i in tmp]

        for i in range(1000):
            if not i in tmp:
                break

        return '{0:03d}'.format(i)


    def check_id_suffix_exists(self, item_id):
        ''' assumes suffix format: #123
            i.e.:
            - at the end of item_id
            - separated by #
            - length == 3 (# doesn't count)
            - contains only numerals

        '''
        spl = item_id.split('#')

        if len(spl) < 2:
            return False

        if len(spl[-1]) != 3:
            return False

        if spl[-1].isnumeric():
            return True


    def extract_citations(self, note):
        ''' extract item_ids from citations

            assumed citation format:
            [[<item_id>|<page number of timestamp>]]
        '''

        cits = re.findall(self.citation_pattern, note)
        cits = [c.rstrip(']').lstrip('[').split('|')[0] for c in cits]

        return set(cits)


    def extract_citations_w_indices(self, note):
        ''' returns a list of tuples:
            (span, text)

            where text contains everything between
            the brackets including them:
            [[<item_id>|<page number of timestamp>]]
        '''
        re_tmp = re.finditer(self.citation_pattern, note)
        cs = [(m.span(), m.group()) for m in re_tmp]
        return cs


    def create_reference_mapping(self, note):
        ''' creates a mapping from position in text
            to reference: item_id and, if available,
            page number or timestamp
        '''
        extr_c = self.extract_citations_w_indices(
            note
        )

        reference_map = {}

        for span, txt in extr_c:
            txt = txt[2:-2].split('|')

            if len(txt) == 1:
                item_id, location = txt[0], None
            else:
                item_id, location = txt[0], txt[1]

            tmp_val = {'item_id':item_id, 'loc':location}
            tmp_map = {i:tmp_val for i in range(*span)}

            reference_map.update(tmp_map)

        return reference_map


    def select_by_tags(self, tags, mode):
        ''' select a slice of meta_data by tags

            tags : a list of tags

            mode : 'and' - selects rows with all tags
                   'or'  - selects rows with at least one tag
        '''

        if mode == 'and':
            mask = self.meta_data['tags'].map(set(tags).issubset())
        else:
            mask = self.meta_data['tags'].map(
                set(tags).intersection() != set([])
            )
        return self.meta_data[mask]


    def init_meta_data(self):
        tmp = {k:pd.Series([], dtype=v) for k,v in self.meta_cols.items()}
        self.meta_data = pd.DataFrame(tmp)
        self.meta_data = self.meta_data.set_index('item_id')


    def init_graph(self):
        self.graph = self.create_graph_from_meta()


    def create_graph_from_meta(self):
        graph = nx.DiGraph()

        if self.meta_data.shape[0] == 0:
            return graph

        graph.add_nodes_from(self.meta_data.index)

        titles = self.meta_data[['title']].to_dict(orient='index')
        nx.set_node_attributes(graph, titles)

        # add connections
        non_empty = self.meta_data[self.meta_data['cited'] != tuple([])]
        for idx, row in non_empty.iterrows():
            for cited in row['cited']:
                graph.add_edge(idx, cited)

        # generate init positions
        # generating positions for all items,
        # even if they won't be showed on the graph
        # seems like a more robust system
        pos = self.generate_random_positions(list(graph.nodes))

        nx.set_node_attributes(graph, pos)

        return graph


    def generate_random_positions(self, nodes):
        df = pd.DataFrame(nodes, columns=['id'])

        df['x'] = np.random.random(len(nodes)) * 300
        df['y'] = np.random.random(len(nodes)) * 300

        df = df.set_index('id')

        return df.to_dict(orient='index')

    def get_items_type(self, item_id):
        return self.meta_data.loc[item_id, 'type']

    def get_items_of_type(self, type_):
        ns = self.meta_data[self.meta_data['type'] == type_].index.to_list()
        return ns


    def get_cited(self, item_id):
        ''' return item_ids of items cited by the item_id '''
        return sorted(list(
            set(nx.all_neighbors(self.graph, item_id)) & set(
                self.graph.successors(item_id))))


    def get_citing(self, item_id):
        ''' return item_ids of items citing item_id '''
        return sorted(list(
            set(nx.all_neighbors(self.graph, item_id)) & set(
                self.graph.predecessors(item_id))))


    def get_notes_subgraph(self):
        # TODO: this function is not used anymore ?
        nodes_e = set(flatten_list(self.graph.edges))
        nodes_n = set(self.get_items_of_type('note'))
        nodes_all = nodes_e | nodes_n

        subg = self.graph.subgraph(nodes_all)

        # find wcc which contain notes
        nodes_plot = [
            list(cc) for cc in
            nx.weakly_connected_components(subg) if
            (len(cc & nodes_n) > 0)
        ]

        nodes_plot = flatten_list(nodes_plot)

        return self.graph.subgraph(nodes_plot)


    def export_graph_for_plotting(self, to_show=None):
        ''' takes a list of ids (to_show),
            adds their neighbours and returns
            data necessary for plotting them
        '''
        # TODO: could add more filtering strategies:
        # - weakly connected component (if only one item?)
        # - -||- and filtering distant components if too many

        # there is no point in plottig everything:
        # the responsiveness plummets
        if (to_show is None) or (len(to_show) > 200):
            return {}, [], [], {}, []

        subg = self.graph

        item_ids = list(subg.nodes)
        edges = list(subg.edges)

        # filtering items to plot
        # show whatever is directly connected to the to_show items
        edges = [e for e in edges if
            (e[0] in to_show) or
            (e[1] in to_show)
        ]
        item_ids = [item for sublist in edges for item in sublist]
        item_ids = list(set(item_ids) | set(to_show))

        pos_x = nx.get_node_attributes(subg, "x")
        pos_y = nx.get_node_attributes(subg, "y")

        pos = np.array(
            [[pos_x[n], pos_y[n]] for n in item_ids if n != ''],
            dtype=float
        )

        adj = np.array([
            [item_ids.index(e[0]), item_ids.index(e[1])] for
            e in edges
        ])

        lines = np.array([(255,255,255,255,1) for e in edges])
        texts = self.meta_data.loc[item_ids]['title'].values
        files = self.meta_data.loc[item_ids]['file'].values
        types = self.meta_data.loc[item_ids]['type'].values
        sizes = self.meta_data.loc[item_ids]['size'].values

        symbols = np.array([self.node_marker_map[t] for t in types])
        sizes_p = self.normalize_sizes(sizes, types)

        items_tags = self.meta_data.loc[item_ids]['tags'].values
        clusters = self.cluster_tags_to_colors(items_tags)

        out_d = {
            'pos':pos, 'adj':adj,
            'pen':lines, 'symbol':symbols,
            'text':texts, 'size':sizes_p,
            'pxMode':False,
        }

        return out_d, item_ids, files, types, clusters


    def normalize_sizes(self, sizes, types):
        '''  '''
        def min_max_scale(s,t):
            tmp = (s-ref[t]['min'])/(ref[t]['max']-ref[t]['min'])

            if np.isnan(tmp) or (tmp == 0):
                tmp = 0.5

            return tmp*10 + 5

        ref = self.get_reference_sizes()
        sizes = [min_max_scale(s,t) for s,t in zip(sizes, types)]

        return sizes


    def cluster_tags_to_colors(self, items_tags):
        aggc = AgglomerativeClustering(
            n_clusters=4, linkage='ward'
        )

        all_tags = sorted(list(set().union(*items_tags)))

        if all_tags == []:
            return np.array([0 for i in items_tags])

        tags_repr = []
        for i_tags in items_tags:
            tags_repr.append([1 if (t in i_tags) else 0 for t in all_tags])

        tags_repr = np.array(tags_repr)

        try:
            clusters = aggc.fit_predict(tags_repr)
        except:
            return np.array([0 for i in items_tags])

        return clusters


    def check_consistency_meta_data_store(self):
        # check if folders match item_ids (indentity)
        errors = []

        folders = [
            i for i in os.listdir(self.config.pkm_store)
            if os.path.isdir(os.path.join(self.config.pkm_store, i))]

        if set(folders) != set(self.meta_data.index):
            errors.append({
                'entries_missing':set(folders) - set(self.meta_data.index),
                'folders_missing':set(self.meta_data.index) - set(folders),
            })

        # check if for each file/note item there is appropriate file
        # ?
        file_meta = self.meta_data[self.meta_data['file'] != '']
        no_file_meta = self.meta_data[self.meta_data['file'] == '']

        for item_id, row in self.meta_data.iterrows():
            if row['file'] == '':
                tmp = os.listdir(self.generate_folder_path(item_id))
                if len(tmp) > 0:
                    errors.append({'spurious_file':item_id})
            else:
                tmp = os.path.exists(self.generate_item_file_path(item_id))
                if not tmp:
                    errors.append({'missing_file':item_id})

        return errors


    def check_consistency_meta_notes(self):
        # check consistency of references from notes with meta
        errors = []

        notes_meta = self.meta_data[self.meta_data['type'] == 'note']
        for item_id, row in notes_meta.iterrows():
            # get and compare citations
            path = self.generate_note_path(item_id)

            try:
                with open(path, 'r') as f:
                    txt = f.readlines()

                refs = self.extract_citations(''.join(txt))

            except FileNotFoundError:
                continue

            if set(refs) != row['cited']:
                errors.append({
                    'item_id':item_id,
                    'refs_missing':set(refs) - set(row['cited']),
                    'refs_spurious':set(row['cited']) - set(refs),
                })

        return errors


    def check_consistency_meta_graph(self):
        # check if item_ids from meta and graph are identical
        # check if references are matching the structure of the graph
        errors = []
        graph = self.create_graph_from_meta()

        if not nx.utils.graphs_equal(self.graph, graph):
            if set(self.graph.nodes) != set(graph):
                errors.append({
                    'nodes_missing':set(graph.nodes)-set(self.graph.nodes),
                    'nodes_spurious':set(self.graph.nodes)-set(graph.nodes),
                })
            else:
                d0 = nx.difference(graph, self.graph)
                d1 = nx.difference(self.graph, graph)
                errors.append({
                    'edges_missing':list(d0.edges),
                    'edges_spurious':list(d1.edges),
                })

        return errors


    def save(self):
        ''' save both meta_data and graph to file '''

        # a modicum of safety
        try:
            os.rename(
                self.config.meta_data,
                self.config.meta_data + '.bckp'
            )
        except FileNotFoundError:
            pass

        try:
            os.rename(
                self.config.graph,
                self.config.graph + '.bckp'
            )
        except FileNotFoundError:
            pass

        # if pickling goes wrong it can make the data unreadable
        # hence the try .. except .. else 
        buffer = io.BytesIO()
        try:
            with io.BytesIO() as f:
                self.meta_data.to_pickle(path=buffer)

        except Exception as e:
            raise e

        else:
            self.meta_data.to_pickle(self.config.meta_data)

            tmp = self.remove_data_from_graph(self.graph, ['title'])
            with open(self.config.graph, 'w+') as out_f:
                json.dump(nx.node_link_data(tmp), out_f)


    def load_meta_data(self, config):
        self.meta_data = pd.read_pickle(config.meta_data)

    def remove_data_from_graph(self, graph, keys):
        ''' returns a graph without data specified in keys
            without affect the input graph (deepcopy)
        '''
        graph = deepcopy(graph)
        for n in graph.nodes:
            for key in keys:
                try:
                    del graph.nodes[n][key]
                except KeyError:
                    pass

        return graph
