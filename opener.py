from messages import Open
import subprocess


class Opener:
    subscribe_to_msgs = [Open]

    format_program_map = {
        'pdf':'xreader',
        'mp3':'mpv',
        'mp4':'mpv',
        'lorien':'Lorien.x86_64',
        'ps':'xreader',
        'epub':'xreader',
        'chm':'xchm',
        'mobi':'fbreader',
        'djvu':'xreader',
        'html':'firefox',
    }

    progs_and_args = {
        'xreader':'--page-index',
        'mpv':'--start',
        'Lorien.x86_64':None,
    }


    def __init__(self, data_manager):
        self.data_manager = data_manager

    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus
        self.comm_nexus.register(self, self.subscribe_to_msgs)

    def on_message(self, msg):
        path = self.data_manager.generate_item_file_path(msg.item_id)
        sfx = path.split('.')[-1]

        if not sfx in self.format_program_map.keys():
            return

        if msg.loc is None:
            cmd = [self.format_program_map[sfx], path]

        else:
            cmd = [
                self.format_program_map[sfx],
                path,
                self.progs_and_args[self.format_program_map[sfx]],
                msg.loc,
            ]

        subprocess.run(cmd)
