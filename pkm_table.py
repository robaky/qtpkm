#! /usr/bin/env python3

import os

import re

import numpy as np
import pandas as pd

from rapidfuzz import process, fuzz, utils

import bibtexparser
from bibtexparser.bibdatabase import BibDatabase
from bibtexparser.bparser import BibTexParser

import pyperclip

from PyQt6 import *
from PyQt6.QtGui import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import Qt, QEvent, QAbstractItemModel, QAbstractTableModel

from fields import *
from messages import *
from flow_layout import FlowLayout
from tag_button import Button3State

import datetime

from text_edit_completion import \
    MyPlainTextEdit, MyCompleter, SearchCompleter


class PKMTable(QWidget):
    subscribe_to_msgs = [
        Preview,
        Select,
        Open,
        DataChanged,
        ItemsDeleted,
        Filter,
    ]

    cols_to_show = [
        'title',
        'read_flag',
        'to_read_flag',
        'tags',
        'creators',
        'type',
        'date_published',
        'date_added',
        'cited',
        'bibtex',
        'file',
        'size',
        'url',
    ]

    req_cols = ['item_id', 'title', 'type']

    cols_hidden = ['bibtex', 'file', 'size', 'url']

    highlight_update_msgs = [Preview, Select, Open]

    def __init__(self, data_manager):
        super().__init__()

        self.data_manager = data_manager

        self.search = QLineEdit()
        self.button_0 = QPushButton('create')
        self.button_1 = QPushButton('edit')
        self.button_2 = QPushButton('import')
        self.button_3 = QPushButton('delete')

        self.table = QTableView()
        self.table.verticalHeader().setVisible(False)

        self.model = TableModel(
            self.prep_data_model()
        )
        self.table.setModel(self.model)

        self.table.horizontalHeader().setSectionResizeMode(
            0, QHeaderView.ResizeMode.Stretch
        )

        self.tags_box = QWidget()
        self.tags_box_layout = QVBoxLayout()
        self.tags_box_layout.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.tags_box.setLayout(self.tags_box_layout)

        self.tags_box_scroll = QScrollArea()
        self.tags_box_scroll.setWidgetResizable(True)
        # in case of future problems, can set scroll policy
        self.tags_box_scroll.setWidget(self.tags_box)

        # idxs: row, col, rowspan, colspan
        layout = QGridLayout()
        layout.addWidget(self.search, 0, 0, 1, 3)
        layout.addWidget(self.button_0, 0, 3, 1, 1)
        layout.addWidget(self.button_1, 0, 4, 1, 1)
        layout.addWidget(self.button_2, 0, 5, 1, 1)
        layout.addWidget(self.table, 1, 0, 1, 3)
        layout.addWidget(self.tags_box_scroll, 1, 3, 1, 3)
        layout.setColumnStretch(0,8)
        self.setLayout(layout)

        # hidden cols
        for c in self.cols_hidden:
            self.table.setColumnHidden(self.cols_to_show.index(c), True)

        # shortcut reactions
        self.shortcut_ctrl_s = QShortcut(QKeySequence('Ctrl+s'), self)
        self.shortcut_ctrl_s.activated.connect(self.on_ctrl_s)

        self.shortcut_ctrl_m = QShortcut(QKeySequence('Ctrl+m'), self)
        self.shortcut_ctrl_m.activated.connect(self.on_ctrl_m)

        self.shortcut_ctrl_e = QShortcut(QKeySequence('Ctrl+e'), self)
        self.shortcut_ctrl_e.activated.connect(self.on_ctrl_e)

        # events
        self.search.textChanged.connect(self.master_filter)

        self.button_0.clicked.connect(self.button_create)
        self.button_1.clicked.connect(self.button_edit)
        self.button_2.clicked.connect(self.button_import)

        self.installEventFilter(self)
        self.search.installEventFilter(self)
        self.table.viewport().installEventFilter(self)

        self.search_comp = SearchCompleter(
            self.generate_search_completions()
        )
        self.search.setCompleter(self.search_comp)

        # used for processing search input
        self.type_proces_map = {
            str:self.match_string,
            set:self.match_iterable,
            list:self.match_iterable,
            bool:self.match_bool,
        }

        # init tags box
        self.populate_tags_box(
            set(self.data_manager.meta_data.index), set())


    def __repr__(self):
        return 'pkm_table'


    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus
        self.comm_nexus.register(self, self.subscribe_to_msgs)


    def on_message(self, message):
        if type(message) is Filter:
            self.apply_filter(message.to_show, message.to_hide)

        elif issubclass(type(message), DataChanged):
            self.on_data_changed(message)

            if type(message) is TagRenamed:
                self.rename_buttons(message.tag_old, message.tag_new)

        elif message.sender == str(self):
            # prevents reacting to Preview/Select/Open
            # originating from the table
            pass

        elif type(message) in self.highlight_update_msgs:
            self.set_current_index(message.item_id)

            row_idx = self.data_manager.meta_data.index.get_loc(
                message.item_id)
            self.table.selectRow(row_idx)            

        else:
            pass


    def prep_data_model(self):
        meta_data_tmp = self.data_manager.meta_data[self.cols_to_show].copy()
        meta_data_tmp['item_id'] = meta_data_tmp.index
        return meta_data_tmp


    def master_filter(self):
        ''' apply filters from both the search field and
            the tag box
        '''
        to_show_1, to_hide_1 = self.process_search()
        to_show_2, to_hide_2 = self.filter_by_tags()

        to_hide = to_hide_1 | to_hide_2
        to_show = set(self.data_manager.meta_data.index) - to_hide

        self.comm_nexus.on_message(
            Filter(
               to_show=to_show,
               to_hide=to_hide,
               sender = str(self),
           )
        )


    def process_search(self):
        '''  '''
        search = self.extract_search_from_str(self.search.text())
        return self.filter_item_ids(search)


    def filter_by_tags(self):
        ''' filter item using the tags box '''
        cur_clicked_buttons = set([
            b for b in self.tags_box.findChildren(
            Button3State) if 
            b._current_state != Button3State.neu_state
        ])
        
        pos_tags = set([
            b.text() for b in cur_clicked_buttons if 
            b._current_state == Button3State.pos_state])
        neg_tags = set([
            b.text() for b in cur_clicked_buttons if 
            b._current_state == Button3State.neg_state])

        to_show = set(self.data_manager.meta_data[
            (self.data_manager.meta_data['tags'].map(
                lambda x: pos_tags.issubset(x))) & \
            (self.data_manager.meta_data['tags'].map(
                lambda x: neg_tags.isdisjoint(x)))
        ].index)

        to_hide = set(self.data_manager.meta_data.index) - to_show

        return to_show, to_hide


    def populate_tags_box(self, to_show, to_hide):
        ''' updates the tags box:
            - keeps the toggled tags
            - keeps the tags present in items left after filtering
            - removes the rest of the tags

            works by clearing all tags and adding them again
        '''
        def connect_button(button):
            ''' connects a new button to event handling functions '''
            button.clicked.connect(
                self.master_filter)
            button.setContextMenuPolicy(
                Qt.ContextMenuPolicy.CustomContextMenu)
            # this sends the item that was clicked to the
            # event handling function
            button.customContextMenuRequested.connect(
                lambda pos, child=button: self.button_right_click(pos, child))

        def copy_button(b):
            ''' copy an old button, including the state '''
            nb = Button3State()
            nb.setText(b.text())
            nb.set_state(b._current_state, b._states)
            connect_button(nb)
            return nb

        cur_clicked_tags = [
            copy_button(b) for b in self.tags_box.findChildren(
            Button3State) if 
            b._current_state != Button3State.neu_state
        ]

        tags_show = self.data_manager.meta_data.loc[
            list(to_show)]['tags'].values
        tags_show = set().union(*tags_show)
        tags_show = tags_show - set([b.text() for b in cur_clicked_tags])
        tags_show = sorted(list(tags_show))

        self.clears_tags_buttons()

        # first adding clicked tags
        for b in cur_clicked_tags:
            b_new = copy_button(b)
            self.tags_box.layout().addWidget(b_new)

        del cur_clicked_tags # remove the last ref to old buttons

        # then the rest in alphabetic order
        for t in tags_show:
            b = Button3State()
            b.setText(t)
            connect_button(b)
            self.tags_box.layout().addWidget(b)


    def clears_tags_buttons(self):
        ''' it seems that this is the most sensible way:
        https://ymt-lab.com/en/post/2021/pyqt5-delete-widget-test/
        https://stackoverflow.com/questions/73258934/python-pyqt6-deletelater-and-child-widgets

        '''
        for b in self.tags_box.findChildren(Button3State):
            b.deleteLater()


    def button_right_click(self, pos, button):
        '''  '''
        popup = TagRenamePopUp(button.text())

        result = popup.exec()

        if result:
            self.comm_nexus.on_message(
                RenameTag(
                    tag_old=popup.tag_old.text(),
                    tag_new=popup.tag_new.text(),
                    sender = str(self)
                ))


    def rename_buttons(self, tag_old, tag_new):
        '''  '''
        for b in self.tags_box.findChildren(Button3State):
            if b.text() == tag_old:
                b.setText(tag_new)


    def extract_search_from_str(self, text):
        '''  '''
        pattern = '|'.join(['%s:' % c for c in self.cols_to_show])
        pattern = '(?:' + pattern + ')'

        matches = [m for m in re.finditer(pattern, text)]

        search = {} 
        
        if len(matches) == 0:
            if text != '':
                search['title'] = text
        else:
            for idx, m in zip(range(len(matches)), matches):
                try:
                    s_end = matches[idx+1].start()
                except:
                    s_end = -1
        
                col = m.group(0)[:-1] # skip colon
                val = text[m.end():s_end] # ends when the next match starts
        
                search[col] = val

        return search


    def filter_item_ids(self, search):
        to_show = set(self.data_manager.meta_data.index)

        if search == {}:
            return to_show, set()

        for key, val in search.items():
            dtype = self.data_manager.meta_cols_true_dtypes[key]
            tmp_f = self.type_proces_map[dtype]

            to_show_tmp = tmp_f(key, val)
            to_show = to_show.intersection(to_show_tmp)

        to_hide = set(self.data_manager.meta_data.index) - to_show

        return to_show, to_hide


    def apply_filter(self, to_show, to_hide):
        # turn item_ids into indexes
        [self.table.setRowHidden(self.model._data.index.get_loc(i), True)
            for i in to_hide]
        [self.table.setRowHidden(self.model._data.index.get_loc(i), False)
            for i in to_show]

        self.populate_tags_box(to_show, to_hide)


    def match_string(self, key, val):
        mask = [o[-1] for o in process.extract(
            val,
            self.data_manager.meta_data[key],
            scorer=fuzz.partial_ratio, 
            score_cutoff=80,
            processor=utils.default_process
        )]
        return set(mask)

    def match_bool(self, key, val):
        mask = self.data_manager.meta_data[key] == bool(val)
        return set(self.data_manager.meta_data[mask].index)

    def match_iterable(self, key, val):
        mask = [o[-1] for o in process.extract(
            val,
            self.data_manager.meta_data[key].astype(str),
            scorer=fuzz.partial_token_sort_ratio, 
            score_cutoff=80,
            processor=utils.default_process
        )]
        return set(mask)


    # event handling and delegation
    def eventFilter(self, source, event):
        flag_select_row = False
        flag_preview = False
        flag_open = False


        if source is self.table.viewport():
            if event.type() == QEvent.Type.MouseButtonPress:
                if event.button() == Qt.MouseButton.LeftButton:
                    index = self.table.indexAt(event.pos())
                    flag_preview = self.on_lmc(index)
                    flag_select_row = True

                elif event.button() == Qt.MouseButton.RightButton:
                    index = self.table.indexAt(event.pos())
                    flag_select_row = True
                    flag_open = True

                else:
                    return super().eventFilter(source, event)

        elif source is self:
            if event.type() == QEvent.Type.KeyRelease:
                arrows = [16777235, 16777237, 16777236, 16777234]

                if event.key() in arrows:
                    flag_select_row = True
                elif event.key() == 16777220:
                    # enter
                    flag_select_row = True
                    flag_open = True
                elif event.key() == 16777223:
                    # delete
                    self.on_delete()
                else:
                    pass

        selected = self.table.selectedIndexes()
        if len(selected) > 0:
            if flag_select_row:
                self.table.selectRow(selected[0].row())
            if flag_preview:
                self.send_preview(selected[0])
            if flag_open:
                self.select_or_open(selected[0])
            return False

        else:
            return super().eventFilter(source, event)


    def send_preview(self, index):
        if self.model._data['type'].iloc[index.row()] == 'note':
            item_id = self.model._data.index[index.row()]
            self.send_simple_message(Preview, item_id)


    def on_lmc(self, index):
        column = self.model._data.columns[index.column()]

        if column in ['to_read_flag', 'read_flag']:
            self.update_flag(index, column)
        else:
            return True

        return False


    def on_delete(self):
        selected = self.table.selectedIndexes()
        rows = list(set([s.row() for s in self.table.selectedIndexes()]))
        if len(rows) == 1:
            item_id = self.model._data.index[rows[0]]
            self.send_multi_message(DeleteItems, [item_id])


    # buttons
    def button_create(self):
        self.create_new_item()

    def button_edit(self):
        selected = self.table.selectedIndexes()

        if len(selected) == 1:
            self.edit_item(selected[0])

    def button_import(self):
        clip_text = pyperclip.paste()

        try:
            bib = self.data_manager.extract_data_from_bibtex_str(clip_text)
        except IndexError:
            return # could pop-up error window or sth

        if len(bib['year']) == 4:
            year = bib['year'] + '-01-01'
        else:
            year = '0001-01-01'

        meta = {
            'item_id':self.data_manager.generate_id_from_bibtex(bib),
            'title':bib['title'],
            'type':'text',
            'creators':CreatorsField.transform(
                bib['author'], 'table', 'meta_data'
            ),
            'date_published':DateField.transform(
                year, 'table', 'meta_data'
            ),
            'bibtex':BibtexField.transform(
                clip_text, 'table', 'meta_data'
            ),
        }

        self.comm_nexus.on_message(
            CreateItem(meta=meta, sender=type(self))
        )


    def on_ctrl_s(self):
        self.data_manager.save()


    def on_ctrl_m(self):
        selected = self.table.selectedIndexes()

        if len(selected) > 1:
            item_ids = [
                self.model._data.index[s.row()] for s in selected]

            self.send_multi_message(MergeItems, item_ids)


    def on_ctrl_e(self):
        selected = self.table.selectedIndexes()

        if len(selected) == 1:
            self.edit_item(selected[0])


    def send_simple_message(self, msg_type, item_id):
        self.comm_nexus.on_message(
            msg_type(
               item_id = item_id,
               sender = str(self)
           )
        )


    def send_multi_message(self, msg_type, item_ids):
        self.comm_nexus.on_message(
            msg_type(
               item_ids = item_ids,
               sender = str(self)
           )
        )

    # actions
    def edit_item(self, index):
        meta = self.model.get_row(index)

        popup = ItemPopUp(
            self.data_manager, meta=meta)
        result = popup.exec()

        if result:
            self.comm_nexus.on_message(
                UpdateItemsMeta(
                    item_id = meta['item_id'], # potentially old item_id
                    meta = popup.meta,
                    sender = str(self)
                )
            )


    def create_new_item(self):
        popup = ItemPopUp(
            self.data_manager)
        result = popup.exec()

        if result:
            self.comm_nexus.on_message(
                CreateItem(
                    meta = popup.meta,
                    sender = str(self)
                )
            )

    def update_flag(self, index, field):
        meta = self.model.get_row(index)

        meta[field] = not meta[field]

        self.comm_nexus.on_message(
            UpdateItemsMeta(
               item_id = meta['item_id'],
               meta = meta,
               sender = str(self)
           )
        )


    def select_or_open(self, index):
        type_ = self.model._data['type'].iloc[index.row()]
        item_id = self.model._data.index[index.row()]

        if type_ == 'note':
            self.send_simple_message(Select, item_id)
        else:
            self.comm_nexus.on_message(
                Open(
                   item_id = item_id,
                   loc = None,
                   sender = str(self)
               )
            )


    def on_data_changed(self, message):
        '''  '''
        df_tmp = self.prep_data_model()
        self.model.reset_model(df_tmp)

        selected = self.table.selectedIndexes()

        if len(selected) == 1:
            item_id = self.data_manager.meta_data.index[selected[0].row()]
        else:
            item_id = None

        self.set_current_index(item_id)


    def set_current_index(self, item_id):
        if not item_id is None:
            row_idx = self.model._data.index.get_loc(item_id)

            self.table.setCurrentIndex(
                self.model.createIndex(row_idx, 0)
            )

    def generate_search_completions(self):
        completions = []

        for col in self.cols_to_show:
            dtype = self.data_manager.meta_cols_true_dtypes[col]

            if (dtype is list) or (dtype is set):

                tmp_c = set([
                    item for sublist in self.model._data[col] 
                    for item in sublist
                ])

                tmp_c = set([col+':'+FieldsDispatcher.transform(
                    col, [v], 'meta_data', 'table')
                    for v in tmp_c])

            else:
                tmp_c = set([col+':'+FieldsDispatcher.transform(
                    col, v, 'meta_data', 'table')
                    for v in self.model._data[col]])
            
            completions.extend(tmp_c)

        return completions



class TableModel(QAbstractTableModel):

    def __init__(self, data):
        super().__init__()
        self._data = data


    def reset_model(self, new_data):
        self.beginResetModel()
        self._data = new_data
        self.endResetModel()

    def data(self, index, role):
        if role == Qt.ItemDataRole.DisplayRole:
            value = self._data.iloc[index.row(), index.column()]
            field = self._data.columns[index.column()]

            FieldsDispatcher.transform(field, value, 'meta_data', 'table')

            return FieldsDispatcher.transform(
                    field, value, 'meta_data', 'table')

    def get_row(self, index):
        return self.row_to_meta(self._data.iloc[index.row()])

    def rowCount(self, index):
        return self._data.shape[0]

    def columnCount(self, index):
        return self._data.shape[1]

    def headerData(self, section, orientation, role):
        # section is the index of the column/row.
        if role == Qt.ItemDataRole.DisplayRole:
            if orientation == Qt.Orientation.Horizontal:
                return str(self._data.columns[section])

            if orientation == Qt.Orientation.Vertical:
                return str(self._data.index[section])

    def row_to_meta(self, row):
        meta = row.to_dict()
        meta['item_id'] = row.name
        return meta


class TagRenamePopUp(QDialog):
    def __init__(self, old_tag):
        super().__init__()

        self.buttons = QDialogButtonBox.StandardButton.Save | QDialogButtonBox.StandardButton.Cancel
        self.button_box = QDialogButtonBox(self.buttons)

        # https://www.pythonguis.com/tutorials/pyqt6-dialogs/
        self.button_box.accepted.connect(self.on_save)
        self.button_box.rejected.connect(self.on_cancel)   

        self.tag_old = QLineEdit()
        self.tag_old.setText(old_tag)
        self.tag_old.setEnabled(False)

        self.tag_new = QLineEdit()

        formLayout = QFormLayout()
        formLayout.addRow("old tag:", self.tag_old)
        formLayout.addRow("new tag:", self.tag_new)

        self.layout = QVBoxLayout()
        self.layout.addLayout(formLayout)
        self.layout.addWidget(self.button_box)
        self.setLayout(self.layout)


    def __repr__(self):
        return 'tag_rename_popup'


    def on_save(self):
        ''' check if fields ok
            if yes, close the popup
            if not, stay and show feedback
        '''
        status, info = self.validate()

        if status:
            self.done(1)
        else:
            self.restore_default_styles()
            for d_ in info:
                d_['field'].setStyleSheet("border: 1px solid red;")


    def on_cancel(self):
        self.done(0)


    def validate(self):
        status = True
        info = []

        tag_new = self.tag_new.text()
        tag_new_wo_ = tag_new.replace('_', '')

        if self.tag_new.text() == '':
            status = False
            info.append({
                'str':"can't rename to an empty tag",
                'field':self.tag_new})

        if not tag_new_wo_.isalnum():
            status = False
            info.append({
                'str':"don't use non-alphanumeric characters except _",
                'field':self.tag_new})

        return status, info



class ItemPopUp(QDialog):

    file_allowed = '<select file>'
    file_blocked = '<valid for file type items only>'
    url_blocked = '<valid for web type items only>'
    url_allowed = '<enter url>'

    def __init__(self, data_manager, meta=None):
        super().__init__()
        self.data_manager = data_manager
        self.item_types = data_manager.item_types
        self.meta = meta

        self.buttons = QDialogButtonBox.StandardButton.Save | QDialogButtonBox.StandardButton.Cancel
        self.button_box = QDialogButtonBox(self.buttons)

        # https://www.pythonguis.com/tutorials/pyqt6-dialogs/
        self.button_box.accepted.connect(self.on_save)
        self.button_box.rejected.connect(self.on_cancel)

        # form entries
        tags_unique = self.data_manager.meta_data['tags'].values
        tags_unique = sorted(list(set().union(*tags_unique)))

        cited_uniqe = list(self.data_manager.meta_data.index)

        tags_completer = MyCompleter(tags_unique)
        cited_completer = MyCompleter(cited_uniqe)

        self.item_id = QLineEdit()
        self.title = QLineEdit()
        self.file = QPushButton()
        self.url = QLineEdit()
        self.tags = MyPlainTextEdit(tags_completer)
        self.type = QComboBox()
        self.creators = QPlainTextEdit()
        self.read_flag = ToggleButton()
        self.to_read_flag = ToggleButton()
        self.date_published = QLineEdit()
        self.date_added = QLineEdit()
        self.cited = MyPlainTextEdit(cited_completer)
        self.bibtex = QPlainTextEdit()

        #tags_validator = TagsValidator()
        #self.tags.setValidator(tags_validator)

        # layouts
        formLayout = QFormLayout()
        formLayout.addRow("item_id:", self.item_id)
        formLayout.addRow("title:", self.title)
        formLayout.addRow("file:", self.file)
        formLayout.addRow("url:", self.url)
        formLayout.addRow("tags:", self.tags)
        formLayout.addRow("type:", self.type)
        formLayout.addRow("creators:", self.creators)
        formLayout.addRow("read_flag:", self.read_flag)
        formLayout.addRow("to_read_flag:", self.to_read_flag)
        formLayout.addRow("date_published:", self.date_published)
        formLayout.addRow("date_added:", self.date_added)
        formLayout.addRow("cited:", self.cited)
        formLayout.addRow("bibtex:", self.bibtex)

        self.layout = QVBoxLayout()
        self.layout.addLayout(formLayout)
        self.layout.addWidget(self.button_box)
        self.setLayout(self.layout)

        # events
        self.file.clicked.connect(self.get_file_path)
        self.type.currentIndexChanged.connect(self.update_type_restrictions)
        self.item_id.textEdited.connect(self.update_meta_restrictions)

        # populate fields if editing an item
        if not self.meta is None:
            self.meta_to_form()
        else:
            self.file.setText(self.file_allowed)

            clip_text = QApplication.clipboard().text()

            # check if string is a bibtex stuff
            parsed_bib = BibtexField.transform(
                clip_text, 'pop_up', 'meta_data')

            if parsed_bib.entries != []:
                bib_flag = True

                self.bibtex.setPlainText(clip_text)

                # also populate the published date if available
                bib_entry = tuple(parsed_bib.entries_dict.values())[0]

                if 'year' in bib_entry.keys():
                    self.date_published.setText(bib_entry['year']+'-01-01')
                else:
                    self.date_published.setText('0001-01-01')

            else:
                self.date_published.setText('0001-01-01')

            self.type.addItems(self.item_types)
            self.type.setCurrentText('text')

            self.date_added.setText(
                FieldsDispatcher.transform(
                    'date_published', datetime.date.today(),
                    'meta_data', 'pop_up')
            )

        self.save_default_styles()

        self.update_type_restrictions()


    def __repr__(self):
        return 'item_popup'


    def on_save(self):
        ''' check if fields ok
            if yes, close the popup
            if not, stay and show feedback
        '''
        status, info = self.validate()

        if status:
            self.meta = self.form_to_meta()
            self.done(1)
        else:
            self.restore_default_styles()
            for d_ in info:
                d_['field'].setStyleSheet("border: 1px solid red;")


    def update_type_restrictions(self):
        type_tmp = self.type.currentText()

        if type_tmp in self.data_manager.item_types_w_files:
            self.file.setEnabled(True)
            self.url.setEnabled(False)
            self.bibtex.setEnabled(True)

        elif type_tmp == 'note':
            self.file.setEnabled(False)
            self.url.setEnabled(False)
            self.bibtex.setEnabled(False)

        elif type_tmp == 'web':
            self.file.setEnabled(False)
            self.url.setEnabled(True)
            self.bibtex.setEnabled(True)

        else:
            raise ValueError('type not recognized: %s' % type_tmp)

        if self.meta is None:
            if type_tmp in self.data_manager.item_types_w_files:
                self.file.setText(self.file_allowed)
                self.url.setText(self.url_blocked)
                self.bibtex.setPlainText('')

            elif type_tmp == 'note':
                self.file.setText(self.file_blocked)
                self.url.setText(self.url_blocked)
                self.bibtex.setPlainText('')

            elif type_tmp == 'web':
                self.file.setText(self.file_blocked)
                self.url.setText(self.url_allowed)
                self.bibtex.setPlainText('')

            else:
                raise ValueError('type not recognized: %s' % type_tmp)
 
        else:
            if type_tmp in self.data_manager.item_types_w_files:
                self.file.setText(self.file_meta_to_form())
                self.url.setText(self.url_blocked)
                self.bibtex_meta_to_form()

            elif type_tmp == 'note':
                self.file.setText(self.file_blocked)
                self.url.setText(self.url_blocked)
                self.bibtex.setPlainText('')

            elif type_tmp == 'web':
                self.file.setText(self.file_blocked)
                self.url.setText(self.meta['url'])
                self.bibtex_meta_to_form()

            else:
                raise ValueError('type not recognized: %s' % type_tmp)


    def update_meta_restrictions(self):
        # this restricts updating url or file
        # if item_id was changed to a new one
        if not self.meta is None:
            if self.meta['item_id'] != self.item_id.text():
                self.file.setText('')
                self.url.setText('')
                self.file.setEnabled(False)
                self.url.setEnabled(False)

            else:
                self.file.setText(self.file_meta_to_form())
                self.url.setText(self.meta['url'])
                self.file.setEnabled(True)
                self.url.setEnabled(True)


    def save_default_styles(self):
        fields = [self.bibtex, self.cited, self.title, self.item_id]
        self.default_styles = []

        for field in fields:
            self.default_styles.append((field, field.styleSheet()))


    def restore_default_styles(self):
        for field, style in self.default_styles:
            field.setStyleSheet(style)


    def validate(self):
        status = True
        info = []

        bib_txt = self.bibtex.toPlainText()
        parsed_bib = BibtexField.transform(
            bib_txt, 'pop_up', 'meta_data')

        cited_txt = self.cited.toPlainText()
        parsed_cited = SetField.transform(
            cited_txt, 'pop_up', 'meta_data')

        if (bib_txt != '') and (parsed_bib.entries == []):
            status = False
            info.append({
                'str':'parsing_issue',
                'field':self.bibtex})

        for item_id in parsed_cited:
            cited_missing = []
            if not item_id in self.data_manager.meta_data.index:
                status = False
                cited_missing.append(item_id)
            if len(cited_missing) > 0:
                info.append({
                    'str':'citations not in database',
                    'missing':cited_missing,
                    'field':self.cited,})

        if self.item_id.text() == '':
            status = False
            info.append({
                'str':'missing item_id',
                'field':self.item_id,})

        if self.title.text() == '':
            status = False
            info.append({
                'str':'missing title',
                'field':self.title,})

        return status, info


    def on_cancel(self):
        self.done(0)


    def get_file_path(self):
        file = QFileDialog.getOpenFileName(
            self, 'Select File', os.path.expanduser('~'))

        self.file.setText(file[0])


    def meta_to_form(self):
        meta = self.meta

        self.item_id.setText(meta['item_id'])
        self.title.setText(meta['title'])

        # can't change item type willy-nilly
        # because it is hard to support and makes no sense
        if meta['type'] in self.data_manager.item_types_w_files:
            self.type.addItems(self.data_manager.item_types_w_files)
        else:
            self.type.addItems([meta['type']])

        self.date_published.setText(
            FieldsDispatcher.transform(
                'date_published', meta['date_published'],
                'meta_data', 'pop_up')
        )
        self.date_added.setText(
            FieldsDispatcher.transform(
                'date_added', meta['date_added'],
                'meta_data', 'pop_up')
        )

        # these fields are ToggleButton(s) and need
        # both status (T/F) and text to show
        self.read_flag.setChecked(meta['read_flag'])
        self.read_flag.setText(str(meta['read_flag']))
        self.to_read_flag.setChecked(meta['to_read_flag'])
        self.to_read_flag.setText(str(meta['to_read_flag']))

        self.type.setCurrentText(meta['type'])

        # setting path to file
        self.file_meta_to_form()

        # setting url
        self.url.setText(meta['url'])

        # these fields need special transform functions
        self.tags.setPlainText(
            SetField.transform(meta['tags'], 'meta_data', 'pop_up'))

        self.creators.setPlainText(
            CreatorsField.transform(meta['creators'], 'meta_data', 'pop_up'))

        self.cited.setPlainText(
            ListField.transform(meta['cited'], 'meta_data', 'pop_up'))

        self.bibtex.setPlainText(
            BibtexField.transform(meta['bibtex'], 'meta_data', 'pop_up'))


    def file_meta_to_form(self):
        self.file.setText(
            self.data_manager.generate_item_file_path(self.meta['item_id']))

    def bibtex_meta_to_form(self):
        self.bibtex.setPlainText(
            BibtexField.transform(self.meta['bibtex'], 'meta_data', 'pop_up'))


    def form_to_meta(self):

        meta = {
            'item_id':self.item_id.text(),
            'title':self.title.text(),
            'date_published':self.date_published.text(),
            'date_added':self.date_added.text(),
            'read_flag':self.read_flag.isChecked(),
            'to_read_flag':self.to_read_flag.isChecked(),
            'type':self.type.currentText(),
            'tags':SetField.transform(
                self.tags.toPlainText(), 'pop_up', 'meta_data'),
            'creators':CreatorsField.transform(
                self.creators.toPlainText(), 'pop_up', 'meta_data'),
            'cited':SetField.transform(
                self.cited.toPlainText(), 'pop_up', 'meta_data'),
            'bibtex':BibtexField.transform(
                self.bibtex.toPlainText(), 'pop_up', 'meta_data'),
        }

        # convert dates
        meta['date_published'] = FieldsDispatcher.transform(
            'date_published', meta['date_published'], 
            'pop_up', 'meta_data'
        )
        meta['date_added'] = FieldsDispatcher.transform(
            'date_added', meta['date_added'], 
            'pop_up', 'meta_data'
        )

        # it seems better to not set 'file' value at all
        # unless a new path has been provided
        
        if self.meta is None:
            # creating new item

            if meta['type'] in self.data_manager.item_types_w_files:

                if self.file.text() == self.file_allowed:
                    pass
                elif self.file.text() == '':
                    pass
                else:
                    meta['file'] = self.file.text()

            elif meta['type'] == 'note':
                meta['note_text'] = ''

            elif meta['type'] == 'web':
                url_val = self.url.text()

                if url_val in [self.url_allowed, self.url_blocked]:
                    pass
                else:
                    meta['url'] = url_val

            else:
                raise ValueError('unrecognized item type: %s' % meta['type'])

        else:
            # editing existing items
            if meta['type'] in self.data_manager.item_types_w_files:
                if self.file.text() in [self.file_allowed, '']:
                    pass
                else:
                    meta['file'] = self.file.text()

            elif meta['type'] == 'note':
                pass

            elif meta['type'] == 'web':
                if self.url.text() in [self.url_allowed, '']:
                    pass
                else:
                    meta['url'] = self.url.text()

            else:
                raise ValueError('unrecognized item type: %s' % meta['type'])

        return meta


class ToggleButton(QPushButton):
    def __init__(self):
        super().__init__()
        self.setCheckable(True)
        self.setChecked(False)
        self.setText('False')
        self.clicked.connect(self.change_state)

    def change_state(self):
        self.setText(str(self.isChecked()))
