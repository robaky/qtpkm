#! /usr/bin/env python3

from datetime import date
from bibtexparser.bparser import BibTexParser
import bibtexparser



def split_field_and_filter(value):
    return [v for v in value.split(' ') if v != '']


class FieldsDispatcher:

    @staticmethod
    def transform(field, value, from_, to_):
        map_ = {
            'item_id':StringField,
            'title':StringField,
            'creators':CreatorsField,
            'tags':SetField,
            'type':StringField,
            'read_flag':BoolField,
            'to_read_flag':BoolField,
            'date_published':DateField,
            'date_added':DateField,
            'cited':SetField,
            'bibtex':BibtexField,
            'file':StringField,
            'size':FloatField,
            'url':StringField,
        }

        return map_[field].transform(value, from_, to_)


class StringField:
    @staticmethod
    def transform(value, from_, to_):
        if from_ == 'meta_data':
            if to_ == 'table':
                return value
            elif to_ == 'pop_up':
                return value
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'table':
            if to_ == 'meta_data':
                return value
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'pop_up':
            if to_ == 'meta_data':
                return value
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        else:
            raise ValueError('from_ value not recognized: %s' % from_)


class FloatField:
    @staticmethod
    def transform(value, from_, to_):
        if from_ == 'meta_data':
            if to_ == 'table':
                return str(value)
            elif to_ == 'pop_up':
                return str(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'table':
            if to_ == 'meta_data':
                return float(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'pop_up':
            if to_ == 'meta_data':
                return float(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        else:
            raise ValueError('from_ value not recognized: %s' % from_)


class BoolField:
    @staticmethod
    def transform(value, from_, to_):
        if from_ == 'meta_data':
            if to_ == 'table':
                return str(value)
            elif to_ == 'pop_up':
                return str(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'table':
            if to_ == 'meta_data':
                return bool(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'pop_up':
            if to_ == 'meta_data':
                return bool(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        else:
            raise ValueError('from_ value not recognized: %s' % from_)



class SetField:
    @staticmethod
    def transform(value, from_, to_):
        if from_ == 'meta_data':
            if to_ == 'table':
                return ' '.join(value)
            elif to_ == 'pop_up':
                return ' '.join(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'table':
            if to_ == 'meta_data':
                return set(split_field_and_filter(value))

            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'pop_up':
            if to_ == 'meta_data':
                return set(split_field_and_filter(value))
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        else:
            raise ValueError('from_ value not recognized: %s' % from_)


class ListField:
    @staticmethod
    def transform(value, from_, to_):
        if from_ == 'meta_data':
            if to_ == 'table':
                return ' '.join(value)
            elif to_ == 'pop_up':
                return ' '.join(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'table':
            if to_ == 'meta_data':
                return split_field_and_filter(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'pop_up':
            if to_ == 'meta_data':
                return split_field_and_filter(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        else:
            raise ValueError('from_ value not recognized: %s' % from_)


class CreatorsField:
    ''' this does not break apart names, surnames, etc.;
        only separates/combines multiple authors using the
        " and " separator
    '''
    @staticmethod
    def transform(value, from_, to_):
        if from_ == 'meta_data':
            if to_ == 'table':
                return ' and '.join(value)
            elif to_ == 'pop_up':
                return ' and '.join(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'table':
            if to_ == 'meta_data':
                return value.split(' and ')
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'pop_up':
            if to_ == 'meta_data':
                return value.split(' and ')
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        else:
            raise ValueError('from_ value not recognized: %s' % from_)


class BibtexField:

    @staticmethod
    def transform(value, from_, to_):
        bp = BibTexParser(interpolate_strings=False)

        if from_ == 'meta_data':
            if to_ == 'table':
                return bibtexparser.dumps(value)
            elif to_ == 'pop_up':
                return bibtexparser.dumps(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'table':
            if to_ == 'meta_data':
                return bp.parse(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'pop_up':
            if to_ == 'meta_data':
                return bp.parse(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        else:
            raise ValueError('from_ value not recognized: %s' % from_)



class DateField:
    @staticmethod
    def transform(value, from_, to_):

        if from_ == 'meta_data':
            if to_ == 'table':
                return value.isoformat()
            elif to_ == 'pop_up':
                return value.isoformat()
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'table':
            if to_ == 'meta_data':
                return date.fromisoformat(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        elif from_ == 'pop_up':
            if to_ == 'meta_data':
                return date.fromisoformat(value)
            else:
                raise ValueError('to_ value not recognized: %s' % to_)

        else:
            raise ValueError('from_ value not recognized: %s' % from_)


