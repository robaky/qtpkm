#! /usr/bin/env python3

import sys
from PyQt6 import *
from PyQt6.QtGui import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import pyqtSignal, Qt


class MyPlainTextEdit(QPlainTextEdit):
    def __init__(self, completer, parent=None):
        super().__init__(parent)

        self.completer = completer
        self.completer.setWidget(self)
        self.completer.insertText.connect(self.insertCompletion)

    def insertCompletion(self, completion):
        tc = self.textCursor()
        extra = (len(completion) - len(self.completer.completionPrefix()))
        tc.movePosition(QTextCursor.MoveOperation.Left)
        tc.movePosition(QTextCursor.MoveOperation.EndOfWord)
        tc.insertText(completion[-extra:])
        self.setTextCursor(tc)
        self.completer.popup().hide()

    def focusInEvent(self, event):
        if self.completer:
            self.completer.setWidget(self)
        QPlainTextEdit.focusInEvent(self, event)

    def keyPressEvent(self, event):
        tc = self.textCursor()
        if event.key() == Qt.Key.Key_Return and self.completer.popup().isVisible():

            self.completer.insertText.emit(self.completer.getSelected())
            self.completer.setCompletionMode(
                QCompleter.CompletionMode.PopupCompletion)
            return

        QPlainTextEdit.keyPressEvent(self, event)
        tc.select(QTextCursor.SelectionType.WordUnderCursor)
        cr = self.cursorRect()

        if len(tc.selectedText()) > 1:
            self.completer.setCompletionPrefix(tc.selectedText())
            popup = self.completer.popup()
            popup.setCurrentIndex(self.completer.completionModel().index(0,0))

            cr.setWidth(self.completer.popup().sizeHintForColumn(0) 
            + self.completer.popup().verticalScrollBar().sizeHint().width())
            self.completer.complete(cr)
        else:
            self.completer.popup().hide()



class MyTextEdit(QTextEdit):
    def __init__(self, completer, parent=None):
        super().__init__(parent)

        self.completer = completer
        self.completer.setWidget(self)
        self.completer.insertText.connect(self.insertCompletion)

        self.completer_flag = False

    def insertCompletion(self, completion):
        tc = self.textCursor()
        extra = (len(completion) - len(self.completer.completionPrefix()))
        tc.movePosition(QTextCursor.MoveOperation.Left)
        tc.movePosition(QTextCursor.MoveOperation.EndOfWord)
        tc.insertText(completion[-extra:])
        self.setTextCursor(tc)
        self.completer.popup().hide()

    def focusInEvent(self, event):
        if self.completer:
            self.completer.setWidget(self)
        QTextEdit.focusInEvent(self, event)

    def keyPressEvent(self, event):

        tc = self.textCursor()
        if event.key() == Qt.Key.Key_Return and self.completer.popup().isVisible():
            self.completer.insertText.emit(self.completer.getSelected())
            self.completer.setCompletionMode(
                QCompleter.CompletionMode.PopupCompletion)

            tc.insertText('|]]')
            self.completer_flag = False

            return

        # this does typing
        QTextEdit.keyPressEvent(self, event)

        tc.select(QTextCursor.SelectionType.WordUnderCursor)

        tc_line = self.textCursor()
        tc_line.select(QTextCursor.SelectionType.LineUnderCursor)

        cr = self.cursorRect()

        if tc_line.selectedText()[-2:] == '[[':
            self.completer_flag = True

        elif tc_line.selectedText()[-2:] == ']]':
            self.completer_flag = False

        elif (len(tc.selectedText()) > 1) and self.completer_flag:
            self.completer.setCompletionPrefix(tc.selectedText())
            popup = self.completer.popup()
            popup.setCurrentIndex(
                self.completer.completionModel().index(0,0))

            cr.setWidth(self.completer.popup().sizeHintForColumn(0)
            + self.completer.popup().verticalScrollBar().sizeHint().width())
            self.completer.complete(cr)

        else:
            self.completer.popup().hide()


class MyCompleter(QCompleter):
    insertText = pyqtSignal(str)

    def __init__(self, completion_list, parent=None):
        QCompleter.__init__(self, completion_list, parent)
        self.highlighted.connect(self.setHighlighted)

    def setHighlighted(self, text):
        self.lastSelected = text

    def getSelected(self):
        return self.lastSelected


class SearchCompleter(QCompleter):
    # TODO: this can be improved by implementing a
    # custom completer model:
    # completions from list model for tags, authors, etc
    # completions from tree model for titles, etc
    insertText = pyqtSignal(str)

    def __init__(self, completion_list, parent=None):
        QCompleter.__init__(self, completion_list, parent)
        self.highlighted.connect(self.setHighlighted)

    def setHighlighted(self, text):
        self.lastSelected = text

    def getSelected(self):
        return self.lastSelected
