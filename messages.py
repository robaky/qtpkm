from dataclasses import dataclass
from typing import Optional



# https://peps.python.org/pep-0557/#id7
# ^ typing any


#
# order an interface actions
#

@dataclass(repr=True)
class Preview:
    item_id: str
    sender: 'typing.Any'

@dataclass(repr=True)
class Select:
    item_id: str
    sender: 'typing.Any'

@dataclass(repr=True)
class Open:
    item_id: str
    loc: str
    sender: 'typing.Any'

@dataclass(repr=True)
class Filter:
    to_show: list
    to_hide: list
    sender: 'typing.Any'

#
# order a data action
#

@dataclass(repr=True)
class CreateItem:
    meta: dict
    sender: 'typing.Any'

@dataclass(repr=True)
class UpdateItemsMeta:
    item_id: str
    meta: dict
    sender: 'typing.Any'

@dataclass(repr=True)
class UpdateNote:
    item_id: str
    text: str
    sender: 'typing.Any'

@dataclass(repr=True)
class GenerateId:
    source: str
    sender: 'typing.Any'
    bib: Optional[dict] = None
    link: Optional[str] = None

@dataclass(repr=True)
class DeleteItems:
    item_ids: list
    sender: 'typing.Any'

@dataclass(repr=True)
class UpdatePositions:
    positions: dict
    sender: 'typing.Any'

@dataclass(repr=True)
class MergeItems:
    item_ids: list
    sender: 'typing.Any'

@dataclass(repr=True)
class RenameTag:
    tag_old: str
    tag_new: str
    sender: 'typing.Any'


#
# inform about performed data action
#

class DataChanged:
    pass

@dataclass(repr=True)
class ItemCreated(DataChanged):
    item_id: str
    sender: 'typing.Any'

@dataclass(repr=True)
class ItemsMetaUpdated(DataChanged):
    item_id: str
    sender: 'typing.Any'

@dataclass(repr=True)
class NoteUpdated(DataChanged):
    item_id: str
    sender: 'typing.Any'

@dataclass(repr=True)
class ItemsMerged(DataChanged):
    item_ids: list
    sender: 'typing.Any'

@dataclass(repr=True)
class ItemsDeleted(DataChanged):
    item_ids: list
    sender: 'typing.Any'


@dataclass(repr=True)
class AddedCitation(DataChanged):
    citing_id: str
    cited_id: str
    sender: 'typing.Any'

@dataclass(repr=True)
class RemovedCitation(DataChanged):
    citing_id: str
    cited_id: str
    sender: 'typing.Any'

@dataclass(repr=True)
class TagRenamed(DataChanged):
    tag_old: str
    tag_new: str
    sender: 'typing.Any'
