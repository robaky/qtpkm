#! /usr/bin/env python3

import sys

import numpy as np

from PyQt6 import *
from PyQt6.QtGui import *
from PyQt6.QtWidgets import *

from pyqtgraph.Qt import QtCore
import pyqtgraph as pg

from messages import *

dtype = [
    ('red',np.ubyte),('green',np.ubyte),
    ('blue',np.ubyte),('alpha', np.ubyte),
    ('width',float)
]


# light - dark
reds = ['#C583AE', '#A45287', '#832C65', '#621046', '#42002B']
yellows = ['#FFE3AA', '#D4B26A', '#AA8539', '#805D15', '#553900']
blues = ['#7C83AF', '#525B92', '#313A75', '#182158', '#080F3A']
greens = ['#ABDD93', '#7AB85C', '#519331', '#306E12', '#184900']
color_colors= [reds, yellows, blues, greens]

background_gray = '#242424'
light_gray = '#bebebe'
v_light_gray = '#E5E4E2'

pg.setConfigOption('background', background_gray)
pg.setConfigOptions(antialias=True)


def hex_to_rgbaw(hex_):
    rgbaw = tuple(list(bytes.fromhex(hex_[1:])) + [255, 2])
    return rgbaw


def mk_pen(rgbaw):
    return pg.mkPen(
        pg.mkColor(rgbaw[:-1]),
        width=rgbaw[-1])



class CitGraph:
    ''' object handling some of the communication and dataprep
        for plotting the pkm items and references
    '''
    node_colors = [c[1] for c in color_colors]
    node_colors_brushes = [pg.mkBrush(c) for c in node_colors]

    default_node_pen = mk_pen(hex_to_rgbaw(background_gray))
    selected_node_pen = mk_pen(hex_to_rgbaw(v_light_gray))
    previewed_node_pen = mk_pen(hex_to_rgbaw(light_gray))

    default_edge = hex_to_rgbaw(light_gray)

    selected_cited = hex_to_rgbaw(color_colors[2][1])
    selected_citing = hex_to_rgbaw(color_colors[3][1])

    previewed_cited = hex_to_rgbaw(color_colors[2][2])
    previewed_citing = hex_to_rgbaw(color_colors[3][2])

    subscribe_to_msgs = [Select, Preview, DataChanged, Filter]


    def __init__(self, data_manager):
        self.data_manager = data_manager
        self.graph_plot = Graph()
        self.setup_params()
        self.load_data()


    def __repr__(self):
        return 'cit_graph'


    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus
        self.graph_plot.register_nexus(comm_nexus)
        self.comm_nexus.register(self, self.subscribe_to_msgs)


    def on_message(self, message):
        if type(message) is Preview:
            # TODO is preview used for anything?
            self.previewed_node = message.item_id
            #self.to_show = [message.item_id]
            #self.load_data('preview')

        elif type(message) is Select:
            self.selected_node = message.item_id
            self.to_show = [message.item_id]
            self.load_data('selection')

        elif issubclass(type(message), DataChanged):
            self.setup_params()
            self.load_data()

        elif type(message) is Filter:
            self.apply_filter(message.to_show, message.to_hide)
            self.load_data()

        else:
            raise ValueError(
                'message type not recognized: %s' % type(message)
            )


    def setup_params(self):
        self.selected_node = None
        self.selected_node_idx = None
        self.cited_edges_idx = []
        self.citing_edges_idx = []

        self.previewed_node = None
        self.previewed_node_idx = None
        self.prev_cited_edges_idx = []
        self.prev_citing_edges_idx = []

        self.to_show = []

        self.graph_plot.item_ids = []


    def apply_filter(self, to_show, to_hide):
        ''' change visibility of items in the graph '''
        self.to_show = to_show


    def apply_preview_or_selection(self, what):
        ''' find nodes and connection affected by
            preview/selection
        '''
        if what == 'selection':
            idx = self.graph_plot.item_ids.index(self.selected_node)
            self.selected_node_idx = idx

            if self.selected_node == self.previewed_node:
                self.previewed_node = None
                self.previewed_node_idx = None

        else:
            idx = self.graph_plot.item_ids.index(self.previewed_node)
            self.previewed_node_idx = idx

        tmp_adj = [list(a) for a in self.graph_plot.adj]

        cited_edges = [
            e for e in tmp_adj if e[0] == idx]
        citing_edges = [
            e for e in tmp_adj if e[1] == idx]

        cited_edges = [
            tmp_adj.index(e) for e in cited_edges]
        citing_edges = [
            tmp_adj.index(e) for e in citing_edges]

        if what == 'selection':
            self.cited_edges_idx = cited_edges
            self.citing_edges_idx = citing_edges
        else:
            self.prev_cited_edges_idx = cited_edges
            self.prev_citing_edges_idx = citing_edges


    def load_data(self, preview_or_selection=None):
        ''' load data from the data_manager and plot '''
        # reset graph
        out = self.data_manager.export_graph_for_plotting(
            to_show=self.to_show)
        out_d, item_ids, files, types, clusters = out

        if out_d == {}:
            return

        self.graph_plot.scatter.clear()

        # save this data in the graph_plot
        # for use in events
        self.graph_plot.item_ids = item_ids        
        self.graph_plot.adj = out_d['adj']
        self.graph_plot.files = files
        self.graph_plot.types = types

        if not preview_or_selection is None:
            self.apply_preview_or_selection(preview_or_selection)

        # skip init in case of empty graph
        if item_ids == []:
            return

        # node_colors
        brushes = [self.node_colors_brushes[c] for c in clusters]
        out_d['brush'] = brushes

        # pen colors
        if (self.selected_node is None) and (self.previewed_node is None):
            out_d['pen'] = mk_pen(self.default_edge)
            out_d['symbolPen'] = self.default_node_pen

        else:
            out_d['pen'], out_d['symbolPen'] = self.prep_colors(out_d)

        # set data
        self.graph_plot.setData(**out_d)


    def prep_colors(self, out_d):
        ''' prepare colors for nodes' outlines and edges '''
        # prep edges
        # mark in- and out- going edges of
        # selected and previewed nodes
        pen = [self.default_edge for e in out_d['adj']]

        for i in self.cited_edges_idx:
            pen[i] = self.selected_cited
        for i in self.citing_edges_idx:
            pen[i] = self.selected_citing

        for i in self.prev_cited_edges_idx:
            pen[i] = self.previewed_cited
        for i in self.prev_citing_edges_idx:
            pen[i] = self.previewed_citing

        pen = np.array(pen, dtype=dtype)

        # prep node outlines
        # mark selected and previewed nodes
        symbolPen = [
            self.default_node_pen for n in self.graph_plot.item_ids]

        if not self.selected_node_idx is None:
            symbolPen[self.selected_node_idx] = self.selected_node_pen
        if not self.previewed_node_idx is None:
            symbolPen[self.previewed_node_idx] = self.previewed_node_pen

        return pen, symbolPen


class Graph(pg.GraphItem):
    ''' plot showing items and references connecting them;
        interactive graph code adapted from example:
        https://www.geeksforgeeks.org/pyqtgraph-creating-graph-item/
    '''

    def __init__(self):
        self.dragPoint = None
        self.dragOffset = None
        self.textItems = []
        pg.GraphItem.__init__(self, hoverable=True)
        self.scatter.sigClicked.connect(self.clicked)
        self.scatter.sigHovered.connect(self.hovered)

        self.previous_hover = None

    def __repr__(self):
        return 'graph'

    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus

    def setData(self, **kwds):
        self.text = kwds.pop('text', [])
        self.data = kwds
        if 'pos' in self.data:
            npts = self.data['pos'].shape[0]
            self.data['data'] = np.empty(npts, dtype=[('index', int)])
            self.data['data']['index'] = np.arange(npts)

        self.setTexts(self.text)
        self.updateGraph()

    def setTexts(self, text):
        for i in self.textItems:
            i.scene().removeItem(i)
        self.textItems = []
        for t in text:
            item = pg.TextItem(t)
            self.textItems.append(item)
            item.setParentItem(self)

    def updateGraph(self):
        pg.GraphItem.setData(self, **self.data)
        for i,item in enumerate(self.textItems):
            item.setPos(*self.data['pos'][i])

    def mouseDragEvent(self, ev):
        if ev.button() != QtCore.Qt.MouseButton.LeftButton:
            ev.ignore()
            return

        if ev.isStart():
            # We are already one step into the drag.
            # Find the point(s) at the mouse cursor
            # when the button was first pressed:
            pos = ev.buttonDownPos()
            pts = self.scatter.pointsAt(pos)

            if len(pts) == 0:
                ev.ignore()
                return

            self.dragPoint = pts[0]
            ind = pts[0].data()[0]
            self.dragOffset = self.data['pos'][ind] - pos

        elif ev.isFinish():
            self.dragPoint = None
            return

        else:
            if self.dragPoint is None:
                ev.ignore()
                return

        ind = self.dragPoint.data()[0]
        self.data['pos'][ind] = ev.pos() + self.dragOffset
        self.updateGraph()
        ev.accept()

        self.send_position_update()


    def clicked(self, pts, ev):
        type_tmp = self.types[ev[0].data()[0]]
        item_id_tmp = self.item_ids[ev[0].data()[0]]

        if type_tmp == 'note':
            self.comm_nexus.on_message(
                Select(
                    item_id = item_id_tmp,
                    sender = str(self),
                )
            )

        else:
            self.comm_nexus.on_message(
                Open(
                    item_id = item_id_tmp,
                    loc = None,
                    sender = str(self),
                )
            )


    def hovered(self, pts, ev):
        # filter out empty hover
        if ev.shape == (0,):
            return

        # get item info
        type_tmp = self.types[ev[0].data()[0]]
        item_id_tmp = self.item_ids[ev[0].data()[0]]

        # filter out if hovered node not changed
        if item_id_tmp == self.previous_hover:
            return

        self.previous_hover = item_id_tmp

        # send message
        if type_tmp == 'note':
            self.comm_nexus.on_message(
                Preview(
                    item_id_tmp,
                    str(self)
                )
            )


    def send_position_update(self):
        msg = UpdatePositions(
            positions=self.prep_position_dict(),
            sender=str(self),
        )
        self.comm_nexus.on_message(msg)


    def prep_position_dict(self):
        return {
            i:{'x':p[0], 'y':p[1]} for i,p in
            zip(self.item_ids, self.data['pos'])
        }
