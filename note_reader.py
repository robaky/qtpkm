#! /usr/bin/env python3

from PyQt6 import *
from PyQt6.QtGui import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import Qt, QEvent, QAbstractItemModel, QAbstractTableModel

from messages import Preview, Select, Open, UpdateNote, UpdateItemsMeta

from text_edit_completion import MyTextEdit, MyCompleter


class NoteReader:
    def __init__(self, mode, data_manager):
        self.reference_map = {}
        self.item_id = ''
        self.old_title = ''

        self.data_manager = data_manager

        self.window = QWidget()

        self.title_field = QLineEdit()
        self.title_field.setReadOnly(True)

        self.button = QPushButton("Reading")
        self.button.setCheckable(True)
        self.button.clicked.connect(self.toggle_edit_mode)

        self.completer = MyCompleter(
            list(data_manager.meta_data.index))
        self.textEdit = MyTextEdit(self.completer)
        self.textEdit.setReadOnly(True)
        self.text_buffer = ''

        self.mode = mode
        if mode == 'preview':
            self.subscribe_to_msgs = [UpdateItemsMeta, Preview]
            self.window.setWindowTitle("preview")
        elif mode == 'select':
            self.subscribe_to_msgs = [UpdateItemsMeta, Select]
            self.window.setWindowTitle("select")
        else:
            raise ValueError('mode not recognized: %s' % str(mode))

        # clickable references
        self.textEdit.selectionChanged.connect(self.open_reference)

        # layout
        self.grid = QGridLayout()
        self.grid.addWidget(self.title_field, 0,0,1,4)
        self.grid.addWidget(self.button, 0,4,1,1)
        self.grid.addWidget(self.textEdit, 1,0,10,5)

        self.window.setLayout(self.grid)

        self.window.show()

        # shortcuts
        self.shortcut_ctrl_s = QShortcut(QKeySequence('Ctrl+s'), self.window)
        self.shortcut_ctrl_s.activated.connect(self.save_note)

        self.shortcut_ctrl_s = QShortcut(QKeySequence('Ctrl+q'), self.window)
        self.shortcut_ctrl_s.activated.connect(self.close_note)


    def __repr__(self):
        return 'NoteReader|%s' % self.mode


    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus
        self.comm_nexus.register(self, self.subscribe_to_msgs)


    def on_message(self, message):
        if type(message) == UpdateItemsMeta:
            if self.item_id == message.item_id:
                self.title_field.setText(message.meta['title'])

        elif type(message) in [Preview, Select]:
            self.load_note(message.item_id)

        else:
            raise ValueError(
                'message type not recognized: %s' % type(message)
            )


    def load_note(self, item_id):
        self.item_id = item_id

        path = self.data_manager.generate_note_path(item_id)

        with open(path, 'r') as f:
            self.text_buffer = ''.join(f.readlines())
            self.textEdit.setPlainText(self.text_buffer)

        if self.button.text() == 'Reading':
            self.textEdit.setMarkdown(self.textEdit.toMarkdown())
        else:
            pass

        # TODO this could be computed and stored for
        # each note (in the same way as the list of references)
        self.reference_map = self.data_manager.create_reference_mapping(
            self.text_buffer
        )

        self.title_field.setText(
            self.data_manager.meta_data.loc[item_id, 'title']
        )

        self.old_title = self.data_manager.meta_data.loc[item_id, 'title']


    def toggle_edit_mode(self):
        if self.button.text() == 'Reading':
            self.button.setText('Editing')
            self.textEdit.setReadOnly(False)
            self.textEdit.setPlainText(self.text_buffer)

            self.title_field.setReadOnly(False)

        else:
            # save the original text, so that it can be restored
            # (rendering markdown is not 100% reversible)
            self.text_buffer = self.textEdit.toPlainText()
            self.reference_map = self.data_manager.create_reference_mapping(
                self.text_buffer
            )

            # also save the note
            self.button.setText('Reading')
            self.textEdit.setReadOnly(True)
            self.textEdit.setMarkdown(self.textEdit.toMarkdown())

            self.title_field.setReadOnly(True)
            self.save_note()


    def save_note(self):
        if self.item_id == '':
            return

        self.comm_nexus.on_message(
            UpdateNote(
                item_id = self.item_id,
                text = self.text_buffer,
                sender = str(self),
            )
        )

        if self.old_title != self.title_field.text():
            self.comm_nexus.on_message(
                UpdateItemsMeta(
                    item_id = self.item_id,
                    meta={
                        'title':self.title_field.text(),
                        'item_id':self.item_id,
                    },
                    sender = str(self),
                )
            )


            # update old title once saved
            self.old_title = self.title_field.text()


    def close_note(self):
        self.item_id = ''

        self.button.setText('Reading')

        self.text_buffer = ''

        self.textEdit.setPlainText('')
        self.textEdit.setReadOnly(True)
        self.textEdit.setMarkdown(self.textEdit.toMarkdown())

        self.title_field.setText('')
        self.title_field.setReadOnly(True)


    def open_reference(self):
        if self.button.text() != 'Reading':
            return

        pos = self.textEdit.textCursor().position()

        if not pos in self.reference_map.keys():
            return

        self.comm_nexus.on_message(Open(
            self.reference_map[pos]['item_id'],
            self.reference_map[pos]['loc'],
            str(self)
        ))
