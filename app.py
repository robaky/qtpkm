#! /usr/bin/env python3

import sys
import argparse

from PyQt6.QtWidgets import QApplication

import qdarktheme

from comm_nexus import  Nexus
from data_manager import DataManager
from note_reader import NoteReader
from pkm import PKM
from opener import Opener

# TODO: can add a lot of usefult things here:
# - usage
# - description & epilog for help msg
parser = argparse.ArgumentParser(
    prog='qtPKM'
)
parser.add_argument(
    '-test',
    action='store_true',
    help='run in manual test mode')
args = parser.parse_args()


if __name__ == '__main__':
    # non-GUI elements
    nexus = Nexus()

    if args.test:
        # setup for manual testing
        from tests import config
        from run_tests import DataManagerTests
        
        # using DataManager from unit tests
        # for manual testing
        dmt = DataManagerTests()

        dmt.tearDown() # making sure to cleanup
        dmt.setUp()

        dmt.setup_for_manual_tests()

        dm = dmt.dm

    else:
        # or not
        import config

        dm = DataManager(config)


    dm.register_nexus(nexus)
    opnr = Opener(dm)
    opnr.register_nexus(nexus)

    # start of the GUI part
    app = QApplication(sys.argv)
    qdarktheme.setup_theme()

    #select_reader = NoteReader('select', dm)
    #select_reader.register_nexus(nexus)
    #preview_reader = NoteReader('preview', dm)
    #preview_reader.register_nexus(nexus)

    pkm_window = PKM(dm)
    pkm_window.register_nexus(nexus)

    print('registered msgs recipients:')
    print(nexus.recipients)

    # exit the app
    exit_code = app.exec()
    dm.save()

    if args.test:
        dmt.tearDown()

    sys.exit(exit_code)
