#! /usr/bin/env python3

import os
import shutil

import unittest

import numpy as np

from data_manager import DataManager
from comm_nexus import  Nexus
from pkm_table import PKMTable

from messages import *
from tests import config


class DataManagerTests(unittest.TestCase):

    def setUp(self):
        os.mkdir('tests/pkm_store')
        self.dm = DataManager(config)


    def tearDown(self):
        try:
            shutil.rmtree('tests/pkm_store')
        except FileNotFoundError:
            pass

        try:
            os.remove('tests/meta.pq')
        except FileNotFoundError:
            pass

        try:
            os.remove('tests/graph_layout.json')
        except FileNotFoundError:
            pass

    def setup_test_items(self):
        items = [
            {
                'item_id':'item_file_1',
                'title':'test item 1',
                'type':'text',
                'path':'./tests/test_file.pdf',
            },
            {
                'item_id':'item_file_2',
                'title':'test item 2',
                'type':'text',
                'path':'./tests/test_file.pdf',
            },
            {
                'item_id':'note_1',
                'title':'test note',
                'type':'note',
                'note_text':
                    'dfg dsfgf ddd [[item_file_1#000|2]],'
                    'f gdfgdfg fgfd [fdg] ',
            },
        ]

        for item in items:
            self.dm.create_item(item)


    def setup_more_conns(self):
        items = [
            {
                'item_id':'item_file_1',
                'title':'test item 1',
                'type':'text',
                'path':'./tests/test_file.pdf',
            },
            {
                'item_id':'item_file_2',
                'title':'test item 2',
                'type':'text',
                'cited':set(['note_1#000']),
                'path':'./tests/test_file.pdf',
            },
            {
                'item_id':'note_1',
                'title':'test note',
                'type':'note',
                'note_text':
                    'dfg dsfgf ddd [[item_file_1#000|2]],'
                    'f gdfgdfg fgfd [fdg] ',
            },
        ]

        for item in items:
            self.dm.create_item(item)


    def setup_consistency_test_set(self):
        # TODO:
        # folders don't match item_id (not enough, excessive)
        # some file/note items have missing files
        # spurious/missing references
        items = [
            {
                'item_id':'item_file_1',
                'title':'test item 1',
                'type':'text',
                'path':'./tests/test_file.pdf',
            },
            {
                'item_id':'item_file_2',
                'title':'test item 2',
                'type':'text',
                'path':'./tests/test_file.pdf',
            },
            {
                'item_id':'note_1',
                'title':'test note 1',
                'type':'note',
                'note_text':'[[item_file_1#000]]',
            },
            {
                'item_id':'note_2',
                'title':'test note 2',
                'type':'note',
                'note_text':'[[item_file_2#000]]',
            },
        ]

        for item in items:
            self.dm.create_item(item)

        # missing item
        os.remove(
            self.dm.generate_item_file_path('item_file_1#000')
        )

        os.remove(
            self.dm.generate_item_file_path('note_1#000')
        )

        # missing folder
        shutil.rmtree(
            self.dm.generate_folder_path('item_file_2#000')
        )

        # spurious folder:
        os.mkdir(
            os.path.join(
                self.dm.generate_folder_path('item_file_1#000'),
                'spurious_folder',
        ))

        # modify the references
        n_path = self.dm.generate_item_file_path('note_2#000')
        os.remove(n_path)

        with open(n_path, 'w+') as f_out:
            f_out.writelines(['[[item_file_1#000]]'])

        # modify the graph
        self.dm.graph.remove_edge('note_2#000', 'item_file_2#000')
        self.dm.graph.add_edge('note_1#000', 'item_file_2#000')


    def generate_random_items(self, n):
        for i in range(n):
            out = {
                'item_id':'r_%i' % i,
                'title':'r_%i' % i,
                'type':'note',
                'note_text':'lorem ipsum [[r_0#000]], [[item_file_1#000]]'
            }

            yield out

    def setup_for_manual_tests(self):
        for i in self.generate_random_items(10):
            self.dm.create_item(i)

        meta = {
            'item_id':'item_file_1',
            'title':'test item',
            'type':'text',
            'path':'./tests/test_file.pdf',
        }
        self.dm.create_item(meta)


    def test_create_item_note(self):
        meta = {
            'item_id':'note_1',
            'title':'test note',
            'type':'note',
            'note_text':
                'dfg dsfgf ddd [[abc1234def#000|22:30:40]],'
                'f gdfgdfg fgfd [fdg] [[lala1234humm#000|34]]',
        }

        self.dm.create_item(meta)

        # created 1 item:
        # items from, citations are not created
        # if mising from the meta_data
        self.assertEqual(
            set(self.dm.meta_data.index),
            {'note_1#000'},
        )

        # added citations 
        self.assertEqual(
            set(self.dm.meta_data.loc['note_1#000', 'cited']),
            {'abc1234def#000', 'lala1234humm#000'},
        )

        # graph state
        self.assertEqual(
            set(self.dm.graph.nodes),
            {'note_1#000', 'abc1234def#000', 'lala1234humm#000'},
        )

        # TODO: test for attributes

        self.assertEqual(
            set(self.dm.graph.edges),
            {
                ('note_1#000', 'abc1234def#000'),
                ('note_1#000', 'lala1234humm#000')
            },
        )


    def test_create_item_file(self):
        meta = {
            'item_id':'item_file_1',
            'title':'test item',
            'type':'text',
            'path':'./tests/test_file.pdf',
        }

        self.dm.create_item(meta)

        # file was copied
        file_flag = os.path.isfile(
            'tests/pkm_store/item_file_1#000/file.pdf'
        )

        self.assertTrue(file_flag)

        # path to folder
        self.assertEqual(
            './tests/pkm_store/item_file_1#000',
            self.dm.generate_folder_path('item_file_1#000')
        )

        # filename
        self.assertEqual(
            'file.pdf',
            self.dm.meta_data.loc['item_file_1#000', 'file']
        )

    def test_create_item_website(self):
        meta = {
            'item_id':'blog_1',
            'title':'test blog',
            'type':'web',
            'url':'https://www.joelonsoftware.com/2002/11/11/'
            'the-law-of-leaky-abstractions/',
        }

        self.dm.create_item(meta)

        # file was copied
        file_flag = os.path.isfile(
            'tests/pkm_store/blog_1#000/index.html'
        )

        self.assertTrue(file_flag)

        # path to folder
        self.assertEqual(
            './tests/pkm_store/blog_1#000',
            self.dm.generate_folder_path('blog_1#000')
        )

        # filename
        self.assertEqual(
            'index.html',
            self.dm.meta_data.loc['blog_1#000', 'file']
        )



    def test_update_metadata_id_change(self):

        self.setup_test_items()

        new_meta = {
            'item_id':'note_2',
            'title':'test note 2',
            'type':'note',
            'cited':set(['item_file_1#000']),
            'file':'note',
        }

        self.dm.update_metadata('note_1#000', new_meta)

        # check meta_data state
        self.assertIn('note_2#000', self.dm.meta_data.index)
        self.assertNotIn('note_1#000', self.dm.meta_data.index)

        self.assertEqual(
            {'item_file_1#000'},
            self.dm.meta_data.loc['note_2#000', 'cited'],
        )

        # check the pkm_store structure
        file_flag = os.path.isfile(
            'tests/pkm_store/note_2#000/note'
        )
        self.assertTrue(file_flag)

        file_flag = os.path.isfile(
            'tests/pkm_store/note_1#000/note'
        )
        self.assertFalse(file_flag)


    def test_update_metadata_id_same(self):
        self.setup_test_items()

        new_meta = {
            'item_id':'note_1#000',
            'title':'test note 2',
            'type':'note',
            'cited':{'item_file_1#000', 'item_file_2#000'},
            'file':'note',
        }

        self.dm.update_metadata('note_1#000', new_meta)


        # check meta_data state
        self.assertIn('test note 2', self.dm.meta_data.title.values)
        self.assertNotIn('test note 1', self.dm.meta_data.title.values)

        self.assertEqual(
            {'item_file_1#000', 'item_file_2#000'},
            self.dm.meta_data.loc['note_1#000', 'cited']
        )

        # check the graph
        self.assertEqual(
            {'item_file_1#000', 'item_file_2#000', 'note_1#000'},
            set(self.dm.graph.nodes)
        )

        self.assertEqual(
            {
                ('note_1#000', 'item_file_1#000'),
                ('note_1#000', 'item_file_2#000'),
            },
            set(self.dm.graph.edges)
        )


    def test_update_note(self):
        self.setup_test_items()

        item_id = 'note_1#000'
        text = 'dfg dsfgf ddd [[item_file_2#000|2]]'

        self.dm.update_note(item_id, text)

        # check the file
        path_ = os.path.join(
            self.dm.generate_folder_path(item_id),
            self.dm.meta_data.loc[item_id, 'file']
        )

        file_flag = os.path.isfile(path_)

        if file_flag:
            with open(path_) as note_f:
                note_txt = note_f.readlines()

            self.assertEqual(note_txt[0], text)

        self.assertTrue(file_flag)

        # check citations in meta_data
        self.assertEqual(
            self.dm.meta_data.loc[item_id, 'cited'],
            {'item_file_2#000'}
        )

        # check citations in graph
        self.assertEqual(
            set(self.dm.graph.edges(item_id)),
            {('note_1#000', 'item_file_2#000')}
        )

    def test_check_mergeability(self):
        # don't allow:
        # - two items with diff files (name? size?)
        # - notes with other types
        # - ???
        pass

    def test_merge_items(self):
        # new setup_test_items (3 items; different meta patterns):
        # 1:2; 3:0; 1:1:1; empty entries, etc.
        # what to do if trying to merge incompatible items?
        pass


    def test_replace_file(self):
        pass


    # TODO: add/remove citation may be removed from the codebase
    def test_remove_citation(self):
        self.setup_test_items()
        self.dm.remove_citation('note_1#000', 'item_file_1#000')

        self.assertEqual(
            self.dm.meta_data.loc['note_1#000', 'cited'],
            set([]),
        )

    def test_add_citation(self):
        self.setup_test_items()
        self.dm.add_citation('note_1#000', 'item_file_2#000')

        self.assertEqual(
            self.dm.meta_data.loc['note_1#000', 'cited'],
            set(['item_file_1#000', 'item_file_2#000']),
        )

    def test_extract_citation_indices(self):
        test_note = 'dfg [[abc|22:30:40]] [fdg] [[lala1234humm#000|34]] sdf'

        cit_idxs = self.dm.extract_citations_w_indices(test_note)

        self.assertEqual(
            cit_idxs[0], ((4, 20), '[[abc|22:30:40]]')
        )

        self.assertEqual(
            cit_idxs[1], ((27, 50), '[[lala1234humm#000|34]]')
        )

    def test_get_notes_subgraph(self):
        self.setup_test_items()
        subg = self.dm.get_notes_subgraph()

        self.assertEqual(
            set(subg.nodes), {'item_file_1#000', 'note_1#000'})
        self.assertEqual(
            set(subg.edges), {('note_1#000', 'item_file_1#000')})


    # TODO: nothing to check here, positions are random and edges are indexes
#    def test_export_graph_for_plotting(self):
#        self.setup_test_items()
#        out = self.dm.export_graph_for_plotting()


    def test_normalize_sizes(self):
        self.setup_test_items()
        in_ss, in_ts = self.dm.meta_data['size'], self.dm.meta_data['type']
        sizes = self.dm.normalize_sizes(in_ss, in_ts)

        self.assertEqual(sizes, [10, 10, 10])


    def test_cluster_tags_to_colors(self):
        tags = np.array([
            {'a', 'b', 'c'}, {'a'}, {'b'}, {'c', 'd'}, {'c', 'a'}
        ])

        clusters = self.dm.cluster_tags_to_colors(tags)

        self.assertEqual(list(clusters), [0, 3, 2, 1, 0])


    def test_get_cited(self):
        self.setup_more_conns()
        cited = self.dm.get_cited('note_1#000')
        self.assertEqual(cited, ['item_file_1#000'])

    def test_get_citing(self):
        self.setup_more_conns()
        cited = self.dm.get_citing('note_1#000')
        self.assertEqual(cited, ['item_file_2#000'])

# TODO can't use this w/o refactoring out GUI parts
#    def test_process_search(self):
#        pkm = PKMTable(self.dm)
#        test_str = 'field1:val field2:val bla bla field3:val'
#        pkm.search.setText(test_str)
#        pkm.process_search()

    def test_check_consistency(self):
        self.setup_consistency_test_set()
        self.dm.save()

        cc_md = self.dm.check_consistency_meta_data_store()
        cc_mn = self.dm.check_consistency_meta_notes()
        cc_mg = self.dm.check_consistency_meta_graph()

        expected_cc_md = [
            {'entries_missing': set(),
            'folders_missing': {'item_file_2#000'}},
            {'missing_file': 'item_file_1#000'},
            {'missing_file': 'item_file_2#000'},
            {'missing_file': 'note_1#000'}]
        expected_cc_mn = [
            {'item_id': 'note_2#000',
            'refs_missing': {'item_file_1#000'},
            'refs_spurious': {'item_file_2#000'}}]
        expected_cc_mg = [{
            'edges_missing': [('note_2#000', 'item_file_2#000')],
            'edges_spurious': [('note_1#000', 'item_file_2#000')]}]

        self.assertEqual(cc_md, expected_cc_md)
        self.assertEqual(cc_mn, expected_cc_mn)
        self.assertEqual(cc_mg, expected_cc_mg)


   
    


if __name__ == '__main__':
    unittest.main()
