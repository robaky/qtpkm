#! /usr/bin/env python3

import sys

import numpy as np

from PyQt6 import *
from PyQt6.QtGui import *
from PyQt6.QtWidgets import *

from pyqtgraph.Qt import QtCore
import pyqtgraph as pg

from messages import Preview, Select, Open, UpdatePositions, DataChanged

from pkm_table import PKMTable
from cit_graph import CitGraph
from flow_layout import FlowLayout
from searchbox import SearchBox



class PKM(QMainWindow):
    def __init__(self, data_manager):
        super().__init__()

        # init widgets        
        self.pkm_table = PKMTable(data_manager)
        self.cit_graph = CitGraph(data_manager)
        self.search_box = SearchBox(data_manager)

        self.plot_widget = pg.PlotWidget()
        self.plot_widget.getPlotItem().hideAxis('bottom')
        self.plot_widget.getPlotItem().hideAxis('left')
        self.plot_widget.setAspectLocked(lock=True, ratio=None)
        self.plot_widget.addItem(self.cit_graph.graph_plot)

        # multi-level layout
        self.current_stack_idx = 0
        self.layout_1 = QStackedLayout()
        layout_2 = QGridLayout()
        layout_2.addWidget(self.plot_widget, 0, 1)
        layout_2.addWidget(self.search_box, 0, 0)
        layout_2.setColumnStretch(1,8)
        layout_widget_2 = QWidget()
        layout_widget_2.setLayout(layout_2)

        self.layout_1.addWidget(self.pkm_table)
        self.layout_1.addWidget(layout_widget_2)
        layout_widget_1 = QWidget()
        layout_widget_1.setLayout(self.layout_1)

        self.setCentralWidget(layout_widget_1)

        # shortcut reactions
        self.shortcut_open = QShortcut(QKeySequence('Alt+1'), self)
        self.shortcut_open.activated.connect(self.switch_idx)

        self.show()

    def __repr__(self):
        return 'pkm_window'

    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus
        self.cit_graph.register_nexus(comm_nexus)
        self.search_box.register_nexus(comm_nexus)
        self.pkm_table.register_nexus(comm_nexus)


    def switch_idx(self):
        self.current_stack_idx = int(not self.current_stack_idx)
        self.layout_1.setCurrentIndex(self.current_stack_idx)
