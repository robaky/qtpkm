from PyQt6 import *
from PyQt6.QtGui import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import Qt

from itertools import cycle

# self.setStyleSheet("background-color : yellow")


class Button3State(QtWidgets.QPushButton):
    neu_state, pos_state, neg_state = range(3)

    def __init__(self, *args, **kwargs):
        super(Button3State, self).__init__(*args, **kwargs)
        self._current_state = Button3State.neu_state
        self._states = cycle([
            Button3State.neu_state,
            Button3State.pos_state,
            Button3State.neg_state])
        self.on_clicked()
        self.clicked.connect(self.on_clicked)

    def update_button(self):
        if self._current_state == Button3State.neu_state:
            self.setStyleSheet("border: 1px solid; border-color:transparent")
        elif self._current_state == Button3State.pos_state:
            self.setStyleSheet("border: 1px solid; border-color:green")
        elif self._current_state == Button3State.neg_state:
            self.setStyleSheet("border: 1px solid; border-color:red")

    def set_state(self, _current_state, _states):
        self._current_state = _current_state
        self._states = _states
        self.update_button()

    @QtCore.pyqtSlot()
    def on_clicked(self):
        self._current_state = next(self._states)
        self.update_button()
