#!/usr/bin/env python3

#from other_utils import debug_logger

from messages import *

class Nexus():
    '''
    Communication nexus - receives and distributes
    messages to/from app components.

    The components should have following methods:

    register_nexus(nexus) : saves the instance of nexus

    on_message(msg) : reacts to message
    '''

    def __init__(self):
        self.recipients = {}

#    @debug_logger
    def register(self, recipient, msgs_types):
        '''
        register recipients, assigning
        them mesages types
        '''
        self.recipients[recipient] = msgs_types

#    @debug_logger
    def on_message(self, msg):
        '''
        pass messages to recipients
        '''

        print('nexus:', type(msg))
#        print(msg)

        for recipient, msgs_types in self.recipients.items():
            if type(msg) in msgs_types:
                recipient.on_message(msg)
            elif type(msg).__bases__[0] in msgs_types:
                # handles the use of superclasses
                # in subscriptions
                recipient.on_message(msg)
            else:
                pass
